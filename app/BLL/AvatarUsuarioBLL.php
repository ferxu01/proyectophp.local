<?php
namespace PROYECTOPHP\app\BLL;

use PROYECTOPHP\app\helpers\UploadFile;

class AvatarUsuarioBLL
{
    use UploadFile;

    private array $allowedFileTypes;
    private string $uploadsDirectory;
    private array $fileProperties;

    public function __construct(array $fileProperties)
    {
        $this->allowedFileTypes = ['image/png', 'image/jpeg'];
        $this->uploadsDirectory = '/assets/img/avatars';
        $this->fileProperties = $fileProperties;
    }

    public function uploadImagen()
    {
        $this->uploadFile(
            $this->allowedFileTypes,
            $this->uploadsDirectory,
            $this->fileProperties
        );
    }
}