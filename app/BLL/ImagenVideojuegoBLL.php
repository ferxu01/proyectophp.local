<?php
namespace PROYECTOPHP\app\BLL;

use PROYECTOPHP\app\helpers\UploadFile;

class ImagenVideojuegoBLL
{
    use UploadFile;

    private array $allowedFileTypes;
    private string $uploadsDirectory;
    private array $fileProperties;

    public function __construct(array $fileProperties, int $idPlataforma)
    {
        $this->allowedFileTypes = ['image/png', 'image/jpeg'];
        $this->uploadsDirectory = '/assets/img/games' . $this->getRutaPlataformaGuardar($idPlataforma);
        $this->fileProperties = $fileProperties;
    }

    /*public function setRutaOriginal(int $idPlataforma)
    {
        return $this->uploadsDirectory . '/original' . $this->getRutaPlataformaGuardar($idPlataforma);
    }

    public function setRutaMiniatura(int $idPlataforma)
    {
        return $this->uploadsDirectory . '/miniatura' . $this->getRutaPlataformaGuardar($idPlataforma);
    }*/

    public function uploadImagen()
    {
        $this->uploadFile(
            $this->allowedFileTypes,
            $this->uploadsDirectory,
            $this->fileProperties
        );
    }

    public function getRutaPlataformaGuardar(int $idPlataforma): string
    {
        $directorio = '';
        switch ($idPlataforma)
        {
            case 1:
                $directorio = '/PS3';
                break;

            case 2:
                $directorio = '/PS4';
                break;

            case 3:
                $directorio = '/3DS';
                break;

            case 4:
                $directorio = '/NINTENDO-SWITCH';
                break;

            case 5:
                $directorio = '/WII-U';
                break;

            case 6:
                $directorio = '/XBOX-360';
                break;

            case 7:
                $directorio = '/XBOX-ONE';
                break;

            default:
                break;
        }
        return $directorio;
    }
}