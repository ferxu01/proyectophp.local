<?php

return [
    'database' => [
        'name' => 'tienda',
        'username' => 'userTienda',
        'password' => 'dwes',
        'connection' => 'mysql:host=proyectophp.local',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'security' => [
        'roles' => [
            'ROL_ADMIN' => 3,
            'ROL_USUARIO' => 2,
            'ROL_ANONIMO' => 1
        ]
    ]
];