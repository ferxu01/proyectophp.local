<?php
namespace PROYECTOPHP\app\controllers;

use Exception;
use PROYECTOPHP\app\BLL\AvatarUsuarioBLL;
use PROYECTOPHP\app\entity\Usuario;
use PROYECTOPHP\app\helpers\FlashMessage;
use PROYECTOPHP\app\helpers\MyLogger;
use PROYECTOPHP\app\helpers\Validation;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\Response;
use PROYECTOPHP\core\Security;

class AuthController
{
    public function registrar()
    {
        $error = FlashMessage::get('error-registro');

        Response::renderView('registro', [
            'error' => $error
        ]);
    }

    public function nuevoUsuario()
    {
        $usuarioRepository = App::getRepository(UsuarioRepository::class);

        try {
            $usuarioRepository->getConnection()->beginTransaction();
            $usernameExiste = false;
            $emailExiste = false;

            $captcha = $_POST['captchaChallenge'];
            $nombre = $_POST['nombre'];
            $apellidos = $_POST['apellidos'];
            $email = $_POST['email'];
            $provincia = $_POST['provincia'];
            $usuario = $_POST['usuario'];
            $password = $_POST['password'];
            $password2 = $_POST['password2'];
            $avatarBLL = new AvatarUsuarioBLL($_FILES['avatar']);

            if (Validation::checkCamposRegistro($nombre, $apellidos, $email, $provincia, $usuario, $password,
                $password2, $_FILES['avatar']['name'], $captcha)) {
                $avatarBLL->uploadImagen();
                $nombreAvatar = $avatarBLL->getUploadedFileName();

                $usuariosBBDD = $usuarioRepository->findAll();
                $passwordEncriptado = Security::encrypt($password);

                $user = new Usuario();
                $user->setNombre($nombre)
                    ->setApellidos($apellidos)
                    ->setEmail($email)
                    ->setProvincia($provincia)
                    ->setAvatar($nombreAvatar)
                    ->setUsuario($usuario)
                    ->setPassword($passwordEncriptado)
                    ->setRol('ROL_USUARIO')
                    ->setIdioma('es_ES')
                    ->setCompras(0)
                    ->setVideojuegosCreados(0)
                    ->setMensajesEnviados(0)
                    ->setMensajesRecibidos(0);

                foreach ($usuariosBBDD as $usuarioActual) {
                    if (strtolower($usuarioActual->getUsuario()) === strtolower($usuario))
                        $usernameExiste = true;

                    if (strtolower($usuarioActual->getEmail()) === strtolower($email))
                        $emailExiste = true;
                }

                if ($usernameExiste === false) {
                    if ($emailExiste === false) {
                        if (!Validation::checkPasswordsIguales($password, $password2)) {
                            FlashMessage::set('error-registro', _('Las contraseñas deben ser iguales'));
                        } else {
                            if ($_SESSION['captcha'] === $captcha) {
                                $usuarioRepository->save($user);

                                //Loguear automáticamente al usuario
                                $usuarioLoguear = App::getRepository(UsuarioRepository::class)
                                    ->findOneBy([
                                        'usuario' => $usuario
                                    ]);
                                $_SESSION['usuario'] = $usuarioLoguear->getId();
                                $_SESSION['captcha'] = null;
                                unset($_SESSION['captcha']);
                            } else {
                                FlashMessage::set('error-registro', _('El captcha no es correcto'));
                                App::get('router')->redirect("registro?nombre=$nombre&apellidos=$apellidos&email=$email&provincia=$provincia&usuario=$usuario");
                            }
                        }
                    } else {
                        FlashMessage::set('error-registro', _('El correo electrónico ya existe'));
                        App::get('router')->redirect("registro?nombre=$nombre&apellidos=$apellidos&email=$email&provincia=$provincia&usuario=$usuario");
                    }
                } else {
                    FlashMessage::set('error-registro', _('El nombre de usuario ya existe'));
                    App::get('router')->redirect("registro?nombre=$nombre&apellidos=$apellidos&email=$email&provincia=$provincia&usuario=$usuario");
                }

                $usuarioRepository->getConnection()->commit();
                App::get('router')->redirect('');

                MyLogger::createLog(
                    'Se ha registrado el usuario ' . $user->getUsuario()
                );
            } else {
                FlashMessage::set('error-registro', _('Los campos no pueden estar vacíos'));
                App::get('router')->redirect("registro?nombre=$nombre&apellidos=$apellidos&email=$email&provincia=$provincia&usuario=$usuario");
            }
        } catch (Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            die('No se ha podido registrar el usuario');
        }


    }

    public function login()
    {
        $usuario = App::get('user');
        if (is_null($usuario)) {
            $error = FlashMessage::get('error-login');
            Response::renderView('login', [
                'error' => $error
            ]);
        }
        else
            App::get('router')->redirect('');
    }

    public function checkLogin($correo = null, $pass = null)
    {
        $email = $correo ?? $_POST['email'];
        $password = $pass ?? $_POST['password'];

        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'email' => $email
        ]);

        if ($usuario !== null) {
            if (Security::checkPassword(
                    $password,
                    $usuario->getPassword()
                ) === true) {
                $_SESSION['usuario'] = $usuario->getId();
                App::get('router')->redirect('');
            } else {
                FlashMessage::set('error-login', _('La contraseña es incorrecta'));
                App::get('router')->redirect('login');
            }
        } else {
            FlashMessage::set('error-login', _('El email es incorrecto'));
            App::get('router')->redirect('login');
        }
    }

    public function logout()
    {
        if (isset($_SESSION['usuario']))
        {
            $_SESSION['usuario'] = null;
            unset($_SESSION['usuario']);
        }

        App::get('router')->redirect('login');
    }

    public function unauthorized()
    {
        header('HTTP/1.1 403 Forbidden', true, 403);
        Response::renderView('403');
    }

    public function generarTextoCaptcha(string $input, int $longitudCadena): string
    {
        $inputLength = strlen($input);
        $generatedString = '';
        for ($i = 0; $i < $longitudCadena; $i++)
        {
            $randomCaracter = $input[mt_rand(0, $inputLength-1)];
            $generatedString .= $randomCaracter;
        }

        return $generatedString;
    }

    public function generarCaptcha()
    {
        $caracteresPermitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        //Crear fondo del captcha
        $imagen = imagecreatetruecolor(200, 50);
        imageantialias($imagen, true);
        $colores = [];
        $rojo = rand(125, 175);
        $verde = rand(125, 175);
        $azul = rand(125, 175);

        for ($i = 0; $i < 5; $i++) {
            $colores[] = imagecolorallocate(
                $imagen, $rojo-20*$i, $verde-20*$i, $azul-20*$i
            );
        }

        imagefill($imagen, 0, 0, $colores[0]);

        for ($i = 0; $i < 10; $i++) {
            imagesetthickness($imagen, rand(2, 10));
            $rectColor = $colores[rand(1, 4)];
            imagerectangle($imagen, rand(-10, 190), rand(-10, 10), rand(-10, 190), rand(40, 60), $rectColor);
        }

        //Generar el captcha entero
        $negro = imagecolorallocate($imagen, 0, 0, 0);
        $blanco = imagecolorallocate($imagen, 255, 255, 255);
        $coloresTexto = [$negro, $blanco];

        $fuentes = ['/assets/fonts/Resolve.ttf', '/assets/fonts/Beckhand.ttf', '/assets/fonts/SweetShine.ttf'];

        $longitudCadena = 7;
        $cadenaCaptcha = $this->generarTextoCaptcha($caracteresPermitidos, $longitudCadena);
        $_SESSION['captcha'] = $cadenaCaptcha;

        for ($i = 0; $i < $longitudCadena; $i++) {
            $espacioLetra = 170/$longitudCadena;
            $inicio = 15;

            imagettftext($imagen, 20, rand(-15, 15), $inicio + $i*$espacioLetra,
                rand(20, 40), $coloresTexto[rand(0, 1)],
                $fuentes[array_rand($fuentes)], $cadenaCaptcha[$i]);
        }

        header('Content-Type: image/png');
        imagepng($imagen);
        imagedestroy($imagen);
    }
}