<?php
namespace PROYECTOPHP\app\controllers;

use Exception;
use PROYECTOPHP\app\entity\Comprar;
use PROYECTOPHP\app\entity\Videojuego;
use PROYECTOPHP\app\helpers\FlashMessage;
use PROYECTOPHP\app\helpers\MyLogger;
use PROYECTOPHP\app\repository\CompraRepository;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\Response;

class ComprarController
{
    public function carroCompra()
    {
        $success = FlashMessage::get('success-comprar');

        $videojuegos = null;
        $videojuegoRepository = null;
        $numArticulos = null;
        $acumuladoPrecioCarro = null;
        $totalPorVideojuego = null;

        if (isset($_SESSION['videojuegos'])) {
            $numArticulos = array_count_values($_SESSION['videojuegos']);
            $acumuladoPrecioCarro = 0;

            $videojuegoRepository = App::getRepository(VideojuegoRepository::class);
            foreach ($numArticulos as $key => $idVideojuego) {
                $videojuegos[] = $videojuegoRepository->findOneBy([
                    'id' => $key
                ]);

                $cantidadPorVideojuego[] = $idVideojuego;
            }

            $index = 0;
            foreach ($videojuegos as $key => $videojuego) {
                $totalPorVideojuego = $videojuego->getPrecio()*$cantidadPorVideojuego[$index];
                $acumuladoPrecioCarro += $totalPorVideojuego;
                $index++;
            }
        }

        Response::renderView('carro-compra', [
            'success' => $success,
            'videojuegos' => $videojuegos,
            'videojuegoRepository' => $videojuegoRepository,
            'numArticulos' => $numArticulos,
            'acumuladoPrecioCarro' => $acumuladoPrecioCarro,
            'totalPorVideojuego' => $totalPorVideojuego
        ]);
    }

    public function addVideojuegoACarro(string $id)
    {
        $_SESSION['videojuegos'][] = $id;

        App::get('router')->redirect('');
    }

    public function eliminarVideojuegoCarro(string $id) //Pendiente
    {
        $numArticulos = $_SESSION['videojuegos'];

        $numArticulos[$id] = null;
        unset($numArticulos[$id]);
        $_SESSION['videojuegos'] = $numArticulos;

        header('Content-Type: application/json');
    }

    public function comprarVideojuegos()
    {
        $compraRepository = App::getRepository(CompraRepository::class);
        $acumCompra = $compraRepository->getUltimaCompra();
        $idCompra = $acumCompra+1;

        try {
            $cantidadPorVideojuegos = array_count_values($_SESSION['videojuegos']);
            $usuario = App::get('user');
            $compraRepository->getConnection()->beginTransaction();

            foreach ($cantidadPorVideojuegos as $idVideojuego => $cantidad) {
                $compra = new Comprar();
                $compra->setIdCompra($idCompra)
                    ->setIdUsuario($usuario->getId())
                    ->setIdVideojuego($idVideojuego)
                    ->setCantidad($cantidad);

                $compraRepository->save($compra);

                //Actualizar el número de ventas del videojuego
                $videojuegoRepository = new VideojuegoRepository();
                /** @var Videojuego $videojuego */
                $videojuego = $videojuegoRepository->find($idVideojuego);
                $videojuego->setVentas($videojuego->getVentas()+$cantidad)
                        ->setStock($videojuego->getStock() - $cantidad);
                $videojuegoRepository->update($videojuego);

                //Actualizar el número de compras del usuario
                $usuarioRepository = new UsuarioRepository();
                $usuario->setCompras(
                    $usuario->getCompras()+1
                );
                $usuarioRepository->update($usuario);
            }

            $compraRepository->getConnection()->commit();

            $_SESSION['videojuegos'][] = null;
            unset($_SESSION['videojuegos']);

            FlashMessage::set('success-comprar', _('Compra realizada correctamente'));
            App::get('router')->redirect('usuario/carro');
        } catch(Exception $exception) {
            $compraRepository->getConnection()->rollBack();
            die('No se ha podido realizar la compra');
        }

        App::get('router')->redirect('videojuegos/nuevo');

        MyLogger::createLog(
            'Se ha realizado la compra'
        );
    }
}