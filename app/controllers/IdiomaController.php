<?php
namespace PROYECTOPHP\app\controllers;

use PROYECTOPHP\core\App;

class IdiomaController
{
    public static function traduce(string $idioma)
    {
        $_SESSION['idioma'] = $idioma;

        App::get('router')->redirect('');
    }

    public static function dameArrayDeIdiomasOrdenado()
    {
        $langs = [];
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        {
            preg_match_all(
                '/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i',
                $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse
            );
            if (count($lang_parse[1]))
            {
                $langs = array_combine($lang_parse[1], $lang_parse[4]);

                foreach ($langs as $lang => $val)
                {
                    if ($val === '') $langs[$lang] = 1;
                }

                arsort($langs, SORT_NUMERIC);
            }
        }

        return $langs;
    }

    public static function getIdioma(): string
    {
        $datosLang = self::dameArrayDeIdiomasOrdenado();
        $idioma = key($datosLang);
        return str_replace('-', '_', $idioma);
    }
}