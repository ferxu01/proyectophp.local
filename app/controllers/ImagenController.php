<?php
namespace PROYECTOPHP\app\controllers;

use PROYECTOPHP\app\entity\Imagen;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;

class ImagenController
{
    public function generarImagenAvatar(string $tipo, string $id)
    {
        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'id' => $id
        ]);

        $image = $usuario->getAvatar();
        $imagen = new Imagen($image, $tipo);

        $imagen->imagenMiniatura();
    }

    public function generarImagenVideojuego(string $tipo, string $plataforma, string $id)
    {
        $videojuego = App::getRepository(VideojuegoRepository::class)->findOneBy([
            'id' => $id
        ]);

        $image = $videojuego->getImagen();
        $imagen = new Imagen($image, $tipo, $plataforma);

        $imagen->imagenMiniatura();
    }

    public function generarMiniMiniaturaAvatar(string $tipo, string $id)
    {
        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'id' => $id
        ]);

        $image = $usuario->getAvatar();
        $imagen = new Imagen($image, $tipo);

        $imagen->imagenMiniMiniatura();
    }

    public function generarMiniaturaVideojuegoCarro(string $tipo, string $plataforma, string $id)
    {
        $videojuego = App::getRepository(VideojuegoRepository::class)->findOneBy([
            'id' => $id
        ]);

        $image = $videojuego->getImagen();
        $imagen = new Imagen($image, $tipo, $plataforma);

        $imagen->imagenMiniMiniatura();
    }
}