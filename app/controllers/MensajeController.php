<?php
namespace PROYECTOPHP\app\controllers;

use Exception;
use PROYECTOPHP\app\entity\Mensaje;
use PROYECTOPHP\app\helpers\FlashMessage;
use PROYECTOPHP\app\helpers\MyLogger;
use PROYECTOPHP\app\repository\MensajeRepository;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\Response;

class MensajeController
{
    public function mostrarMensajes()
    {
        $correoEmisor = [];
        $usuario = App::get('user');
        $mensajes = App::getRepository(MensajeRepository::class)->findBy([
            'idReceptor' => $usuario->getId()
        ]);

        foreach ($mensajes as $mensaje) {
            $correoEmisor[] = App::getRepository(UsuarioRepository::class)->findOneBy([
                'id' => $mensaje->getIdEmisor()
            ])->getEmail();
        }

        $success = FlashMessage::get('success-enviar-mensaje');

        Response::renderView('mis-mensajes', [
            'success' => $success,
            'mensajes' => $mensajes,
            'correoEmisor' => $correoEmisor
        ]);
    }

    public function nuevoMensaje(string $id = null)
    {
        if (!is_null($id)) {
            $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
                'id' => $id
            ]);
            $usuarioReceptor = $usuario->getEmail();

            $error = FlashMessage::get('error-enviar-mensaje');

            Response::renderView('nuevo-mensaje', [
                'error' => $error,
                'usuarioReceptor' => $usuarioReceptor
            ]);
        } else {
            $error = FlashMessage::get('error-enviar-mensaje');

            Response::renderView('nuevo-mensaje', [
                'error' => $error
            ]);
        }
    }

    public function enviarMensaje()
    {
        $mensajeRepository = App::getRepository(MensajeRepository::class);

        try {
            $idMensaje = $mensajeRepository->getUltimoMensaje()+1;

            $usuario = App::get('user');
            $mensajeRepository->getConnection()->beginTransaction();

            $destinatario = $_POST['destinatario'];
            $asunto = $_POST['asunto'];
            $textoMensaje = $_POST['textoMensaje'];

            $receptor = App::getRepository(UsuarioRepository::class)->findOneBy([
                'email' => $destinatario
            ]);

            if (!empty($destinatario) && !empty($asunto) && !empty($textoMensaje)){
                if (!is_null($receptor)) {
                    $mensaje = new Mensaje();
                    $mensaje->setId($idMensaje)
                        ->setNombre($usuario->getNombre())
                        ->setApellidos($usuario->getApellidos())
                        ->setEmail($usuario->getEmail())
                        ->setIdEmisor($usuario->getId())
                        ->setIdReceptor($receptor->getId())
                        ->setTexto($textoMensaje)
                        ->setAsunto($asunto)
                        ->setRespuesta(0);

                    $mensajeRepository->save($mensaje);

                    //Actualizar número de mensajes enviados al usuario emisor
                    $usuario->setMensajesEnviados(
                        $usuario->getMensajesEnviados()+1
                    );
                    App::getRepository(UsuarioRepository::class)->update($usuario);

                    //Actualizar número de mensajes recibidos al usuario receptor
                    $receptor->setMensajesRecibidos(
                        $receptor->getMensajesRecibidos()+1
                    );
                    App::getRepository(UsuarioRepository::class)->update($receptor);

                    FlashMessage::set('success-enviar-mensaje', _('Mensaje enviado con éxito'));
                } else {
                    FlashMessage::set('error-enviar-mensaje', _('No existe el destinatario con ese correo electrónico'));
                    App::get('router')->redirect('mensaje/nuevo');
                }
            } else {
                FlashMessage::set('error-enviar-mensaje', _('Los campos no pueden estar vacíos'));
                App::get('router')->redirect('mensaje/nuevo');
            }

            $mensajeRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $mensajeRepository->getConnection()->rollBack();
            die('No se ha podido enviar el mensaje');
        }

        App::get('router')->redirect('mis-mensajes');

        MyLogger::createLog(
            'El usuario ' . $usuario->getId() . ' ha enviado un nuevo mensaje'
        );
    }

    public function responderMensaje(string $id)
    {
        $mensaje = App::getRepository(MensajeRepository::class)->findBy([
            'id' => $id
        ])[0];

        $correoEmisor = App::getRepository(UsuarioRepository::class)->findOneBy([
            'id' => $mensaje->getIdEmisor()
        ])->getEmail();
        $asunto = $mensaje->getAsunto();

        Response::renderView('responder-mensaje', [
            'correoEmisor' => $correoEmisor,
            'asunto' => $asunto,
            'id' => $id
        ]);
    }

    public function enviarRespuesta(string $id)
    {
        $mensajeRepository = App::getRepository(MensajeRepository::class);

        try {
            $usuario = App::get('user');
            $mensajeRepository->getConnection()->beginTransaction();

            $destinatario = $_POST['destinatario'];
            $asunto = $_POST['asunto'];
            $respuesta = $_POST['textoMensaje'];

            $receptor = App::getRepository(UsuarioRepository::class)->findOneBy([
                'email' => $destinatario
            ]);

            $idMensaje = App::getRepository(MensajeRepository::class)->findBy([
                'id' => $id
            ])[0]->getId();

            if (isset($destinatario) && isset($asunto) && (isset($respuesta) && !empty($respuesta))) {
                $mensaje = new Mensaje();
                $mensaje->setId($idMensaje)
                    ->setNombre($usuario->getNombre())
                    ->setApellidos($usuario->getApellidos())
                    ->setEmail($usuario->getEmail())
                    ->setIdEmisor($usuario->getId())
                    ->setIdReceptor($receptor->getId())
                    ->setTexto($respuesta)
                    ->setAsunto($asunto)
                    ->setRespuesta(1);

                $mensajeRepository->save($mensaje);

                //Actualizar número de mensajes enviados al usuario emisor
                $usuario->setMensajesEnviados(
                    $usuario->getMensajesEnviados()+1
                );
                App::getRepository(UsuarioRepository::class)->update($usuario);

                //Actualizar número de mensajes recibidos al usuario receptor
                $receptor->setMensajesRecibidos(
                    $receptor->getMensajesRecibidos()+1
                );
                App::getRepository(UsuarioRepository::class)->update($receptor);

                FlashMessage::set('success-enviar-mensaje', _('Mensaje enviado con éxito'));
            } else {
                FlashMessage::set('', '');
            }

            $mensajeRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $mensajeRepository->getConnection()->rollBack();
            die('No se ha podido enviar el mensaje');
        }

        App::get('router')->redirect('mis-mensajes');
    }
}