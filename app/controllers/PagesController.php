<?php
namespace PROYECTOPHP\app\controllers;

use PROYECTOPHP\app\entity\HeaderSlider;
use PROYECTOPHP\app\repository\PlataformaRepository;
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\Response;
use Swift_Mailer;
use Swift_Message;
use Swift_SendmailTransport;
use Exception;

class PagesController
{
    private function setDatosPaginacion(): array
    {
        $tamanyoPaginas = 6;
        if (isset($_GET['pag']))
            $paginaActual = $_GET['pag'];
        else
            $paginaActual = 1;

        $empezarDesde = ($paginaActual-1)*$tamanyoPaginas;
        $numFilas = App::getRepository(VideojuegoRepository::class)->getNumRegistros();

        return [$tamanyoPaginas, $empezarDesde, ceil($numFilas/$tamanyoPaginas)];
    }

    public function index()
    {
        $tamanyoPaginas = $this->setDatosPaginacion()[0];
        $empezarDesde = $this->setDatosPaginacion()[1];
        $totalPaginas = $this->setDatosPaginacion()[2];

        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();
        $videojuegos = App::getRepository(VideojuegoRepository::class)->findAllLimit($empezarDesde, $tamanyoPaginas);

        Response::renderView('index', [
            'plataformas' => $plataformas,
            'videojuegos' => $videojuegos,
            'totalPaginas' => $totalPaginas
        ]);
    }

    public function videojuegosPorPlataforma(string $plataforma)
    {
        $tamanyoPaginas = $this->setDatosPaginacion()[0];
        $empezarDesde = $this->setDatosPaginacion()[1];
        $totalPaginas = $this->setDatosPaginacion()[2];

        $plataformaFormateada = implode(' ', explode('-', $plataforma));

        $plataform = App::getRepository(PlataformaRepository::class)->findOneBy([
            'nombre' => $plataformaFormateada
        ]);

        $videogames[$plataform->getNombre()] = App::getRepository(
            VideojuegoRepository::class)->findByLimit($empezarDesde, $tamanyoPaginas, [
            'plataforma' => $plataform->getId()
        ]);

        $banner = HeaderSlider::getBannerPlataforma($plataformaFormateada);
        $headerSlider = new HeaderSlider($banner, true);

        Response::renderView('videojuegoCategoria', [
            'videogames' => $videogames,
            'headerSlider' => $headerSlider,
            'totalPaginas' => $totalPaginas
        ]);
    }

    public function mostrarVideojuegosUsuario(string $id)
    {
        $desde = $this->setDatosPaginacion()[1];
        $tamanyoPaginas = $this->setDatosPaginacion()[0];
        $totalPaginas = $this->setDatosPaginacion()[2];

        $videojuegos = App::getRepository(VideojuegoRepository::class)->findByLimit($desde, $tamanyoPaginas, [
            'usuario' => $id
        ]);

        Response::renderView('index', [
            'videojuegos' => $videojuegos,
            'totalPaginas' => $totalPaginas
        ]);
    }

    public function sobreNosotros()
    {
        Response::renderView('sobre-nosotros');
    }

    public function contacto()
    {
        Response::renderView('contacto');
    }

    public function enviarCorreo()
    {
        try {
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $asunto = $_POST['asunto'];
            $mensaje = $_POST['mensaje'];

            $transport = new Swift_SendmailTransport('/usr/sbin/sendmail -bs');

            $mailer = new Swift_Mailer($transport);

            $mensajeCorreo = (new Swift_Message())
                ->setSubject($asunto)
                ->setFrom([$email => $nombre])
                ->setTo($email)
                ->setBody($mensaje);

            $resultado = $mailer->send($mensajeCorreo);
        } catch (Exception $exception) {
            die('No se envió el correo');
        }

        App::get('router')->redirect('contacto');
    }

    public function notFound()
    {
        header('HTTP/1.1 404 Not Found', true, 404);
        Response::renderView('404');
    }
}