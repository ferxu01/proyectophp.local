<?php
namespace PROYECTOPHP\app\controllers;

use PROYECTOPHP\app\BLL\AvatarUsuarioBLL;
use PROYECTOPHP\app\helpers\FlashMessage;
use PROYECTOPHP\app\helpers\MyLogger;
use PROYECTOPHP\app\helpers\Validation;
use PROYECTOPHP\core\App;
use Exception;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\core\Response;
use PROYECTOPHP\core\Security;

class UsuarioController
{
    public function miPerfil()
    {
        $_usuario = App::get('user');
        $usuario = App::getRepository(UsuarioRepository::class)->find($_usuario->getId());

        Response::renderView('mi-perfil', [
            'usuario' => $usuario
        ]);
    }

    public function gestionUsuarios()
    {
        $roles = [
            'ADMIN' => 'ROL_ADMIN',
            'USUARIO' => 'ROL_USUARIO'
        ];
        $usuarios = App::getRepository(UsuarioRepository::class)
            ->findAll();
        $error = FlashMessage::get('error-delete-user');
        $success = FlashMessage::get('success-editar-rol');

        Response::renderView('gestion-usuarios', [
            'usuarios' => $usuarios,
            'roles' => $roles,
            'success' => $success,
            'error' => $error
        ]);
    }

    public function borrarUsuario(string $id)
    {
        try {
            $usuarioRepository = App::getRepository(UsuarioRepository::class);
            $usuarioRepository->getConnection()->beginTransaction();

            $user = $usuarioRepository->find($id);

            if ($user->getCompras() === 0
                && $user->getVideojuegosCreados() === 0
                && $user->getMensajesEnviados() === 0
                && $user->getMensajesRecibidos() === 0) {
                $usuarioRepository->delete($user);

                $usuarioRepository->getConnection()->commit();
            } else {
                FlashMessage::set('error-delete-user',
                    _('No se pueden eliminar usuarios que hayan creado videojuegos, realizado compras o hayan enviado o recibido mensajes'));
            }
        } catch(Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            return false;
        }

        header('Content-Type: application/json');

        echo json_encode([
            'error' => false,
            'mensaje' => "El usuario con id $id se ha eliminado correctamente"
        ]);

        return true;
    }

    public function formEditarPassword()
    {
        $error = FlashMessage::get('error-editar-password');
        $success = FlashMessage::get('success-editar-password');

        Response::renderView('editar-password', [
            'error' => $error,
            'success' => $success
        ]);
    }

    public function editarPassword()
    {
        $usuarioRepository = App::getRepository(UsuarioRepository::class);

        try {
            $usuario = App::get('user');
            $usuarioRepository->getConnection()->beginTransaction();

            $passActual = $_POST['passwordActual'];
            $nuevaPass = $_POST['nuevaPassword'];

            if (!empty($passActual) && !empty($nuevaPass)) {
                if (Security::checkPassword($passActual, $usuario->getPassword())) {
                    if (!Validation::checkPasswordsIguales($passActual, $nuevaPass)) {
                        $passwordEncriptado = Security::encrypt($nuevaPass);

                        $usuario->setPassword($passwordEncriptado);
                        $usuarioRepository->update($usuario);

                        FlashMessage::set('success-editar-password', _('Contraseña editada con éxito'));
                    } else {
                        FlashMessage::set('error-editar-password', _('Las contraseñas no pueden ser las mismas'));
                    }
                } else {
                    FlashMessage::set('error-editar-password', _('La contraseña actual es incorrecta'));
                }
            } else {
                FlashMessage::set('error-editar-password', _('Los campos no pueden estar vacíos'));
            }
            $usuarioRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            die('No se ha podido editar la contraseña');
        }

        App::get('router')->redirect('usuario/editar/password');
    }

    public function editarRol(string $id)
    {
        $usuarioRepository = App::getRepository(UsuarioRepository::class);

        try {
            $usuarioRepository->getConnection()->beginTransaction();

            $usuario = $usuarioRepository->findOneBy([
                'id' => $id
            ]);
            $rolUsuario = $_POST['rolUsuario'] ?? $usuario->getRol();

            $usuario->setRol($rolUsuario);
            $usuarioRepository->update($usuario);

            $usuarioRepository->getConnection()->commit();

            FlashMessage::set('success-editar-rol', _('El rol se ha editado correctamente'));
            App::get('router')->redirect('usuarios/gestion');
        } catch(Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            die('No se ha podido editar el rol del usuario');
        }

        MyLogger::createLog(
            'Se ha editado el rol del usuario con id ' . $id
        );
    }

    public function formEditarAvatar()
    {
        $success = FlashMessage::get('success-editar-avatar');
        $error = FlashMessage::get('error-editar-avatar');

        Response::renderView('editar-avatar', [
            'success' => $success,
            'error' => $error
        ]);
    }

    public function editarAvatar()
    {
        $usuarioRepository = App::getRepository(UsuarioRepository::class);

        try {
            $usuarioLogueado = App::get('user');
            $usuarioRepository->getConnection()->beginTransaction();

            $usuario = $usuarioRepository->findOneBy([
                'id' => $usuarioLogueado->getId()
            ]);

            $avatarBLL = new AvatarUsuarioBLL($_FILES['avatar']);

            if (!empty($_FILES['avatar']['name'])) {
                $avatarBLL->uploadImagen();
                $avatarUsuario = $avatarBLL->getUploadedFileName();

                $usuario->setAvatar($avatarUsuario);
                $usuarioRepository->update($usuario);

                $usuarioRepository->getConnection()->commit();

                FlashMessage::set('success-editar-avatar', _('El avatar se ha editado correctamente'));
            } else {
                FlashMessage::set('error-editar-avatar', _('Selecciona el avatar que quieres editar'));
            }

            App::get('router')->redirect('usuario/editar/avatar');
        } catch(Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            die('No se ha podido editar el avatar del usuario');
        }

        MyLogger::createLog(
            'Se ha editado el avatar del usuario con id ' . $usuarioLogueado->getId()
        );
    }

    public function filtrarRol()
    {
        $usuarioRepository = App::getRepository(UsuarioRepository::class);

        try {
            $usuarioRepository->getConnection()->beginTransaction();

            if (isset($_POST['rolUsuario']))
                $rol = $_POST['rolUsuario'] ?? 'ROL_ADMIN';

            $usuarios = $usuarioRepository->findBy([
                'rol' => $rol
            ]);

            Response::renderView('gestion-usuarios', [
                'usuarios' => $usuarios
            ]);

            $usuarioRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $usuarioRepository->getConnection()->rollBack();
            die('No se ha podido filtrar por rol de usuario');
        }
    }
}