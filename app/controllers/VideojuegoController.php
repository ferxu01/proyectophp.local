<?php
namespace PROYECTOPHP\app\controllers;

use PROYECTOPHP\app\BLL\ImagenVideojuegoBLL;
use PROYECTOPHP\app\helpers\FlashMessage;
use PROYECTOPHP\app\helpers\Validation;
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\app\entity\Plataforma;
use PROYECTOPHP\app\entity\Videojuego;
use PROYECTOPHP\app\helpers\MyLogger;
use PROYECTOPHP\app\repository\PlataformaRepository;
use PROYECTOPHP\app\repository\VideojuegoRepository;
use Exception;
use PROYECTOPHP\core\Response;

class VideojuegoController
{
    public function categoria()
    {
        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();

        foreach ($plataformas as $plataforma)
        {
            $videogames[$plataforma->getNombre()] = App::getRepository(
                VideojuegoRepository::class)->findBy([
                    'plataforma' => $plataforma->getId()
            ]);
        }

        Response::renderView('categoria', [
            'plataformas' => $plataformas,
            'videogames' => $videogames
        ]);
    }

    public function formNuevoVideojuego()
    {
        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();

        $success = FlashMessage::get('success-crear-videojuego');
        $error = FlashMessage::get('error-crear-videojuego');

        Response::renderView('videojuegos', [
            'plataformas' => $plataformas,
            'error' => $error,
            'success' => $success
        ]);
    }

    public function filtrarVideojuegos(string $redireccion)
    {
        $plataforma = $_POST['plataformaFiltrada'];

        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();
        $videojuegos = App::getRepository(VideojuegoRepository::class)
            ->filterByDate([], $plataforma);

        Response::renderView($redireccion, [
            'plataformas' => $plataformas,
            'videojuegos' => $videojuegos
        ]);
    }

    public function buscar()
    {
        $request = file_get_contents('php://input');
        $videogames = json_decode($request);

        $arrayResultado = (array)$videogames;

        $json = App::getRepository(VideojuegoRepository::class)->searchBy([
            'nombre' => $arrayResultado['search'],
            'descripcion' => $arrayResultado['search']
        ]);

        $videojuegoRepositoryActual = App::getRepository(VideojuegoRepository::class);
        $plataformaRepository = App::getRepository(PlataformaRepository::class);

        foreach ($json as $index => $videojuego) {
            $nomUsuario = $videojuegoRepositoryActual->getUsuario($videojuego)->getUsuario();
            $nomPlataforma = strtoupper($videojuegoRepositoryActual->createSlug(
                $plataformaRepository->find($videojuego->getPlataforma())->getNombre()));
            $nomVideojuego = $videojuegoRepositoryActual->createSlug($videojuego->getNombre());
            $idVideojuego = $videojuego->getId();
            $nuevaPlataforma = $videojuegoRepositoryActual->createSlug($nomPlataforma);
            $urlImagen = "/generaImagen/videojuego/$nomPlataforma/$idVideojuego";
            $linkDetallesVideojuego = "/videojuegos/$nuevaPlataforma/$nomVideojuego/$idVideojuego";

            $nuevoJson[$index] = $videojuego->toArray();

            $nuevoJson[$index]['imagen'] = $urlImagen;
            $nuevoJson[$index]['usuario'] = $nomUsuario;
            $nuevoJson[$index]['plataforma'] = $nomPlataforma;
            $nuevoJson[$index]['id'] = $idVideojuego;
            $nuevoJson[$index]['linkDetallesVideojuego'] = $linkDetallesVideojuego;
        }

        header('Content-Type: application/json');

        echo json_encode($nuevoJson);
    }

    public function detallesVideojuego(string $plataforma, string $nombre, string $id)
    {
        $videojuego = App::getRepository(VideojuegoRepository::class)->find($id);

        Response::renderView('videojuego', [
            'videojuego' => $videojuego
        ]);
    }

    public function misVideojuegos()
    {
        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();
        $videojuegos = App::getRepository(VideojuegoRepository::class)
            ->findBy([
                'usuario' => App::get('user')->getId()
            ]);

        $success = FlashMessage::get('success-add-videojuego-carro');

        Response::renderView('mis-videojuegos', [
            'videojuegos' => $videojuegos,
            'plataformas' => $plataformas,
            'success' => $success
        ]);
    }

    public function nuevoVideojuego()
    {
        $videojuegoRepository = App::getRepository(VideojuegoRepository::class);

        try {
            $usuario = App::get('user');
            $videojuegoRepository->getConnection()->beginTransaction();

            $nombre = $_POST['nombre'];
            $descripcion = $_POST['descripcion'];
            $plataforma = $_POST['plataforma'] ?? 1;
            $precio = $_POST['precio'];
            $imagenBLL = new ImagenVideojuegoBLL($_FILES['imagen'], $plataforma);

            if (Validation::checkCamposVideojuego($nombre, $descripcion, $plataforma, $precio, $_FILES['imagen']['name'])) {
                $imagenBLL->uploadImagen();
                $imagen = $imagenBLL->getUploadedFileName();

                $videojuego = new Videojuego();
                $videojuego->setNombre($nombre)
                    ->setDescripcion($descripcion)
                    ->setImagen($imagen)
                    ->setPrecio($precio)
                    ->setPlataforma($plataforma)
                    ->setUsuario($usuario->getId())
                    ->setVentas(0)
                ->setStock(10);

                $videojuegoRepository->save($videojuego);

                //Actualizar número de videojuegos de la plataforma
                $plataformaRepository = new PlataformaRepository();
                /** @var Plataforma $plataforma */
                $plataforma = $plataformaRepository->find($plataforma);
                $plataforma->setNumVideojuegos(
                    $plataforma->getNumVideojuegos()+1
                );
                $plataformaRepository->update($plataforma);

                //Actualizar número de videojuegos que ha creado el usuario
                $usuario->setVideojuegosCreados(
                    $usuario->getVideojuegosCreados()+1
                );
                App::getRepository(UsuarioRepository::class)->update($usuario);

                $videojuegoRepository->getConnection()->commit();

                FlashMessage::set('success-crear-videojuego', _('Videojuego creado con éxito'));

                App::get('router')->redirect('videojuegos/nuevo');
            } else {
                FlashMessage::set('error-crear-videojuego', _('Los campos no pueden estar vacíos'));
            }
        } catch(Exception $exception) {
            $videojuegoRepository->getConnection()->rollBack();
            die('No se ha podido insertar el videojuego');
        }

        App::get('router')->redirect("videojuegos/nuevo?nombre=$nombre&descripcion=$descripcion&precio=$precio");

        MyLogger::createLog(
            'Se ha insertado un nuevo videojuego llamado ' . $videojuego->getNombre()
        );
    }

    private function borrarVideojuego(string $id)
    {
        try {
            $usuario = App::get('user');
            $videojuegoRepository = App::getRepository(VideojuegoRepository::class);
            $videojuegoRepository->getConnection()->beginTransaction();

            $videojuego = $videojuegoRepository->find($id);
            if ($usuario->getId() !== $videojuego->getUsuario())
                FlashMessage::set('error-editar-videojuego', _('No puedes borrar videojuegos que no hayas creado'));

            $videojuegoRepository->delete($videojuego);

            //Actualizar número de videojuegos creados con la plataforma
            $plataformaRepository = new PlataformaRepository();
            /** @var Plataforma $plataforma */
            $plataforma = $plataformaRepository->find($videojuego->getPlataforma());
            $plataforma->setNumVideojuegos(
                $plataforma->getNumVideojuegos()-1
            );
            $plataformaRepository->update($plataforma);

            //Actualizar número de videojuegos creados por el usuario
            $usuario->setVideojuegosCreados(
                $usuario->getVideojuegosCreados()-1
            );
            App::getRepository(UsuarioRepository::class)->update($usuario);

            $videojuegoRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $videojuegoRepository->getConnection()->rollBack();
            return false;
        }
        return true;
    }

    public function borrar(string $id)
    {
        $this->borrarVideojuego($id);

        App::get('router')->redirect('videojuegos');
    }

    public function borrarJson(string $id)
    {
        $this->borrarVideojuego($id);

        header('Content-Type: application/json');

        echo json_encode([
            'error' => false,
            'mensaje' => "El videojuego con id $id se ha eliminado correctamente"
        ]);
    }

    public function formEditarVideojuego(string $id)
    {
        $usuario = App::get('user');
        $videojuego = App::getRepository(VideojuegoRepository::class)->findOneBy([
            'id' => $id
        ]);

        if (!is_null($videojuego)) {
            if ($usuario->getId() !== $videojuego->getUsuario() && $usuario->getRol() !== 'ROL_ADMIN')
                App::get('router')->redirect('mis-videojuegos');
        } else {
            App::get('router')->redirect('mis-videojuegos');
        }

        $plataformas = App::getRepository(PlataformaRepository::class)->findAll();

        $error = FlashMessage::get('error-editar-videojuego');
        $success = FlashMessage::get('success-editar-videojuego');
        
        Response::renderView('editar-videojuego', [
            'error' => $error,
            'success' => $success,
            'videojuego' => $videojuego,
            'plataformas' => $plataformas
        ]);
    }

    public function editarVideojuego(string $id)
    {
        $videojuegoRepository = App::getRepository(VideojuegoRepository::class);

        try {
            $usuario = App::get('user');
            $videojuegoRepository->getConnection()->beginTransaction();
            $videojuegoEditar = App::getRepository(VideojuegoRepository::class)->findOneBy([
                'id' => $id
            ]);

            $nombre = $_POST['nuevoNombre'];
            $descripcion = $_POST['nuevaDescripcion'];
            $plataforma = $_POST['nuevaPlataforma'] ?? 1;
            $precio = $_POST['nuevoPrecio'] ?? 0;
            $imagenBLL = new ImagenVideojuegoBLL($_FILES['nuevaImagen'], $plataforma);

            if (Validation::checkCamposVideojuego($nombre, $descripcion, $plataforma, $precio, $_FILES['nuevaImagen']['name'])) {
                $imagenBLL->uploadImagen();
                $imagen = $imagenBLL->getUploadedFileName();

                $videojuego = new Videojuego();
                $videojuego->setId($id)
                    ->setNombre($nombre)
                    ->setDescripcion($descripcion)
                    ->setImagen($imagen)
                    ->setPrecio($precio)
                    ->setPlataforma($plataforma)
                    ->setUsuario($usuario->getId())
                    ->setVentas($videojuegoEditar->getVentas())
                    ->setStock($videojuegoEditar->getStock());

                $videojuegoRepository->update($videojuego);

                FlashMessage::set('success-editar-videojuego', _('Videojuego editado con éxito'));
            } else {
                FlashMessage::set('error-editar-videojuego', _('Los campos no pueden estar vacíos'));
            }
            $videojuegoRepository->getConnection()->commit();

            App::get('router')->redirect('videojuegos/editar/' . $id);
        } catch(Exception $exception) {
            $videojuegoRepository->getConnection()->rollBack();
            die('No se ha podido editar el videojuego');
        }

        MyLogger::createLog(
            'Se ha editado el videojuego con id ' . $id
        );
    }
}