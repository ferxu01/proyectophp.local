<?php
namespace PROYECTOPHP\app\entity;

use DateTime;
use PROYECTOPHP\core\database\IEntity;

class Comprar implements IEntity
{
    private int $idCompra;
    private $fechaCompra;
    private int $idUsuario;
    private int $idVideojuego;
    private int $cantidad;

    /**
     * Comprar constructor.
     */
    public function __construct()
    {
        if (is_null($this->fechaCompra))
            $this->fechaCompra = new DateTime();
        else
            $this->fechaCompra = new DateTime($this->fechaCompra);

    }

    /**
     * @return int
     */
    public function getIdCompra(): int
    {
        return $this->idCompra;
    }

    /**
     * @param int $idCompra
     * @return Comprar
     */
    public function setIdCompra(int $idCompra): Comprar
    {
        $this->idCompra = $idCompra;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCompra()
    {
        return $this->fechaCompra;
    }

    /**
     * @param mixed $fechaCompra
     * @return Comprar
     */
    public function setFechaCompra($fechaCompra)
    {
        $this->fechaCompra = $fechaCompra;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdUsuario(): int
    {
        return $this->idUsuario;
    }

    /**
     * @param int $idUsuario
     * @return Comprar
     */
    public function setIdUsuario(int $idUsuario): Comprar
    {
        $this->idUsuario = $idUsuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdVideojuego(): int
    {
        return $this->idVideojuego;
    }

    /**
     * @param int $idVideojuego
     * @return Comprar
     */
    public function setIdVideojuego(int $idVideojuego): Comprar
    {
        $this->idVideojuego = $idVideojuego;
        return $this;
    }

    /**
     * @return int
     */
    public function getCantidad(): int
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     * @return Comprar
     */
    public function setCantidad(int $cantidad): Comprar
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'idCompra' => $this->getIdCompra(),
            'fechaCompra' => $this->getFechaCompra()->format('Y-m-d H:i:s'),
            'idUsuario' => $this->getIdUsuario(),
            'idVideojuego' => $this->getIdVideojuego(),
            'cantidad' => $this->getCantidad()
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        // TODO: Implement getId() method.
    }
}