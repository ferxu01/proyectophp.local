<?php
namespace PROYECTOPHP\app\entity;

class HeaderSlider
{
    const RUTA_IMAGEN_FONDO = 'assets/img/hero/';
    private string $imagen;
    private bool $videojuego;

    /**
     * HeaderSlider constructor.
     * @param string $imagen
     * @param bool $videojuego
     */
    public function __construct(string $imagen, bool $videojuego = false)
    {
        $this->imagen = $imagen;
        $this->videojuego = $videojuego;
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->imagen;
    }

    /**
     * @return bool
     */
    public function isVideojuego(): bool
    {
        return $this->videojuego;
    }

    public function getPathImagenBanner() : string
    {
        return self::RUTA_IMAGEN_FONDO . $this->imagen;
    }

    public static function getBannerPlataforma(string $plataforma): string
    {
        $banner = '';
        switch (strtolower($plataforma))
        {
            case 'ps3':
                $banner = '/banner-ps3.jpg';
                break;

            case 'ps4':
                $banner = '/banner-ps4.jpg';
                break;

            case '3ds':
                $banner = '/banner-3ds.jpg';
                break;

            case 'nintendo switch':
                $banner = '/banner-nintendo-switch.jpg';
                break;

            case 'wii u':
                $banner = '/banner-wii-u.jpg';
                break;

            case 'xbox 360':
                $banner = '/banner-xbox-360.jpg';
                break;

            case 'xbox one':
                $banner = '/banner-xbox-one.jpg';
                break;
        }

        return $banner;
    }
}