<?php
namespace PROYECTOPHP\app\entity;


class Imagen
{
    private $archivo;
    private $imagen;
    private $dimensiones;
    private string $tipo;
    private string $extension;

    public function __construct($archivo, string $tipo, string $plataforma = '')
    {
        $this->tipo = $tipo;
        $this->extension = strpos($this->archivo, 'jpg') ? 'jpg' : 'png';

        if ($this->tipo === 'videojuego')
            $this->archivo = $this->setRuta($archivo, $plataforma);
        elseif ($this->tipo === 'avatar') {
            $this->archivo = $this->setRutaAvatar($archivo);
        }

        if (strpos($this->archivo, 'png')) {
            $this->imagen = imagecreatefrompng($this->archivo);
        } elseif (strpos($this->archivo, 'jpg')) {
            $this->imagen = imagecreatefromjpeg($this->archivo);
        }

        $this->dimensiones = getimagesize($this->archivo);
    }

    public function imagenMiniMiniatura()
    {
        $dimensionx = 120;
        $dimensiony = 120;

        $miniatura = imagecreatetruecolor($dimensionx, $dimensiony);
        imagecopyresampled($miniatura, $this->imagen, 0, 0, 0, 0,
            $dimensionx, $dimensiony, $this->dimensiones[0], $this->dimensiones[1]);

        if ($this->extension === 'png') {
            header("Content-Type: image/png");
            imagepng($miniatura);
        } elseif ($this->extension === 'jpg') {
            header("Content-Type: image/jpeg");
            imagejpeg($miniatura);
        }
    }

    public function imagenMiniatura()
    {
        $dimensionx = 370;
        $dimensiony = 403;

        $miniatura = imagecreatetruecolor($dimensionx, $dimensiony);
        imagecopyresampled($miniatura, $this->imagen, 0, 0, 0, 0,
            $dimensionx, $dimensiony, $this->dimensiones[0], $this->dimensiones[1]);

        if ($this->extension === 'png') {
            header("Content-Type: image/png");
            imagepng($miniatura);
        } elseif ($this->extension === 'jpg') {
            header("Content-Type: image/jpeg");
            imagejpeg($miniatura);
        }
    }

    public function setRuta(string $imagen, string $plataforma)
    {
        return './assets/img/games/' . $plataforma . '/' . $imagen;
    }

    public function setRutaAvatar(string $imagen)
    {
        return './assets/img/avatars/' . $imagen;
    }
}