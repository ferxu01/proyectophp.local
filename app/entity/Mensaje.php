<?php
namespace PROYECTOPHP\app\entity;

use DateTime;
use PROYECTOPHP\core\database\IEntity;

class Mensaje implements IEntity
{
    private int $id;
    private string $nombre;
    private string $apellidos;
    private string $email;
    private string $asunto;
    private string $texto;
    private $fechaEnvio;
    private int $idEmisor;
    private int $idReceptor;
    private int $respuesta;

    /**
     * Mensaje constructor.
     */
    public function __construct()
    {
        if (is_null($this->fechaEnvio))
            $this->fechaEnvio = new DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Mensaje
     */
    public function setId(int $id): Mensaje
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Mensaje
     */
    public function setNombre(string $nombre): Mensaje
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     * @return Mensaje
     */
    public function setApellidos(string $apellidos): Mensaje
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Mensaje
     */
    public function setEmail(string $email): Mensaje
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getAsunto(): string
    {
        return $this->asunto;
    }

    /**
     * @param string $asunto
     * @return Mensaje
     */
    public function setAsunto(string $asunto): Mensaje
    {
        $this->asunto = $asunto;
        return $this;
    }

    /**
     * @return string
     */
    public function getTexto(): string
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     * @return Mensaje
     */
    public function setTexto(string $texto): Mensaje
    {
        $this->texto = $texto;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaEnvio()
    {
        return $this->fechaEnvio;
    }

    /**
     * @param mixed $fechaEnvio
     * @return Mensaje
     */
    public function setFechaEnvio($fechaEnvio)
    {
        $this->fechaEnvio = $fechaEnvio;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdEmisor(): int
    {
        return $this->idEmisor;
    }

    /**
     * @param int $idEmisor
     * @return Mensaje
     */
    public function setIdEmisor(int $idEmisor): Mensaje
    {
        $this->idEmisor = $idEmisor;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdReceptor(): int
    {
        return $this->idReceptor;
    }

    /**
     * @param int $idReceptor
     * @return Mensaje
     */
    public function setIdReceptor(int $idReceptor): Mensaje
    {
        $this->idReceptor = $idReceptor;
        return $this;
    }

    /**
     * @return int
     */
    public function getRespuesta(): int
    {
        return $this->respuesta;
    }

    /**
     * @param int $respuesta
     * @return Mensaje
     */
    public function setRespuesta(int $respuesta): Mensaje
    {
        $this->respuesta = $respuesta;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'apellidos' => $this->getApellidos(),
            'email' => $this->getEmail(),
            'asunto' => $this->getAsunto(),
            'texto' => $this->getTexto(),
            'fechaEnvio' => $this->getFechaEnvio()->format('Y-m-d H:i:s'),
            'idEmisor' => $this->getIdEmisor(),
            'idReceptor' => $this->getIdReceptor(),
            'respuesta' => $this->getRespuesta()
        ];
    }
}