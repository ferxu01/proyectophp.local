<?php
namespace PROYECTOPHP\app\entity;

use PROYECTOPHP\core\database\IEntity;

class Plataforma implements IEntity
{
    private int $id;
    private string $nombre;
    private int $numVideojuegos;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Plataforma
     */
    public function setId(int $id): Plataforma
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Plataforma
     */
    public function setNombre(string $nombre): Plataforma
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumVideojuegos(): int
    {
        return $this->numVideojuegos;
    }

    /**
     * @param int $numVideojuegos
     * @return Plataforma
     */
    public function setNumVideojuegos(int $numVideojuegos): Plataforma
    {
        $this->numVideojuegos = $numVideojuegos;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'numVideojuegos' => $this->getNumVideojuegos()
        ];
    }
}