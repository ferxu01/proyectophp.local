<?php
namespace PROYECTOPHP\app\entity;

use PROYECTOPHP\core\database\IEntity;
use DateTime;

class Usuario implements IEntity
{
    const RUTA_USER_AVATAR = '../assets/img/avatars/';
    private int $id;
    private string $nombre;
    private string $apellidos;
    private string $email;
    private string $provincia;
    private string $avatar;
    private string $usuario;
    private string $password;
    private string $rol;
    private $fechaAlta;
    private string $idioma;
    private int $compras;
    private int $videojuegosCreados;
    private int $mensajesEnviados;
    private int $mensajesRecibidos;

    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        if (is_null($this->fechaAlta))
            $this->fechaAlta = new DateTime();
        else
            $this->fechaAlta = new DateTime($this->fechaAlta);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre(string $nombre): Usuario
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getApellidos(): string
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     * @return Usuario
     */
    public function setApellidos(string $apellidos): Usuario
    {
        $this->apellidos = $apellidos;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Usuario
     */
    public function setEmail(string $email): Usuario
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getProvincia(): string
    {
        return $this->provincia;
    }

    /**
     * @param string $provincia
     * @return Usuario
     */
    public function setProvincia(string $provincia): Usuario
    {
        $this->provincia = $provincia;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return Usuario
     */
    public function setAvatar(string $avatar): Usuario
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsuario(): string
    {
        return $this->usuario;
    }

    /**
     * @param string $usuario
     * @return Usuario
     */
    public function setUsuario(string $usuario): Usuario
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Usuario
     */
    public function setPassword(string $password): Usuario
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getRol(): string
    {
        return $this->rol;
    }

    /**
     * @param string $rol
     * @return Usuario
     */
    public function setRol(string $rol): Usuario
    {
        $this->rol = $rol;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFechaAlta(): DateTime
    {
        return $this->fechaAlta;
    }

    /**
     * @param DateTime $fechaAlta
     * @return Usuario
     */
    public function setFechaAlta(DateTime $fechaAlta): Usuario
    {
        $this->fechaAlta = $fechaAlta;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdioma(): string
    {
        return $this->idioma;
    }

    /**
     * @param string $idioma
     * @return Usuario
     */
    public function setIdioma(string $idioma): Usuario
    {
        $this->idioma = $idioma;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompras(): int
    {
        return $this->compras;
    }

    /**
     * @param int $compras
     * @return Usuario
     */
    public function setCompras(int $compras): Usuario
    {
        $this->compras = $compras;
        return $this;
    }

    /**
     * @return int
     */
    public function getVideojuegosCreados(): int
    {
        return $this->videojuegosCreados;
    }

    /**
     * @param int $videojuegosCreados
     * @return Usuario
     */
    public function setVideojuegosCreados(int $videojuegosCreados): Usuario
    {
        $this->videojuegosCreados = $videojuegosCreados;
        return $this;
    }

    /**
     * @return int
     */
    public function getMensajesEnviados(): int
    {
        return $this->mensajesEnviados;
    }

    /**
     * @param int $mensajesEnviados
     * @return Usuario
     */
    public function setMensajesEnviados(int $mensajesEnviados): Usuario
    {
        $this->mensajesEnviados = $mensajesEnviados;
        return $this;
    }

    /**
     * @return int
     */
    public function getMensajesRecibidos(): int
    {
        return $this->mensajesRecibidos;
    }

    /**
     * @param int $mensajesRecibidos
     * @return Usuario
     */
    public function setMensajesRecibidos(int $mensajesRecibidos): Usuario
    {
        $this->mensajesRecibidos = $mensajesRecibidos;
        return $this;
    }

    public function getRutaAvatar(): string
    {
        return self::RUTA_USER_AVATAR . $this->avatar;
    }

    public function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'apellidos' => $this->getApellidos(),
            'email' => $this->getEmail(),
            'provincia' => $this->getProvincia(),
            'avatar' => $this->getAvatar(),
            'usuario' => $this->getUsuario(),
            'password' => $this->getPassword(),
            'rol' => $this->getRol(),
            'fechaAlta' => $this->getFechaAlta()->format('Y-m-d H:i:s'),
            'idioma' => $this->getIdioma(),
            'compras' => $this->getCompras(),
            'videojuegosCreados' => $this->getVideojuegosCreados(),
            'mensajesEnviados' => $this->getMensajesEnviados(),
            'mensajesRecibidos' => $this->getMensajesRecibidos()

        ];
    }
}