<?php
namespace PROYECTOPHP\app\entity;

use PROYECTOPHP\core\database\IEntity;
use DateTime;

class Videojuego implements IEntity
{
    const RUTA_VIDEOJUEGO = '/assets/img/games/';
    private int $id;
    private string $nombre;
    private string $descripcion;
    private string $imagen;
    private int $precio;
    private $fechaCreacion;
    private int $plataforma;
    private int $usuario;
    private int $ventas;
    private int $stock;

    /**
     * Videojuego constructor.
     */
    public function __construct()
    {
        if (is_null($this->fechaCreacion))
            $this->fechaCreacion = new DateTime();
        else
            $this->fechaCreacion = new DateTime($this->fechaCreacion);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Videojuego
     */
    public function setId(int $id): Videojuego
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Videojuego
     */
    public function setNombre(string $nombre): Videojuego
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Videojuego
     */
    public function setDescripcion(string $descripcion): Videojuego
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->imagen;
    }

    /**
     * @param string $imagen
     * @return Videojuego
     */
    public function setImagen(string $imagen): Videojuego
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrecio(): int
    {
        return $this->precio;
    }

    /**
     * @param int $precio
     * @return Videojuego
     */
    public function setPrecio(int $precio): Videojuego
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFechaCreacion(): DateTime
    {
        return $this->fechaCreacion;
    }

    /**
     * @param DateTime $fechaCreacion
     * @return Videojuego
     */
    public function setFechaCreacion(DateTime $fechaCreacion): Videojuego
    {
        $this->fechaCreacion = $fechaCreacion;
        return $this;
    }

    /**
     * @return int
     */
    public function getPlataforma(): int
    {
        return $this->plataforma;
    }

    /**
     * @param int $plataforma
     * @return Videojuego
     */
    public function setPlataforma(int $plataforma): Videojuego
    {
        $this->plataforma = $plataforma;
        return $this;
    }

    /**
     * @return int
     */
    public function getUsuario(): int
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     * @return Videojuego
     */
    public function setUsuario(int $usuario): Videojuego
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getVentas(): int
    {
        return $this->ventas;
    }

    /**
     * @param int $ventas
     * @return Videojuego
     */
    public function setVentas(int $ventas): Videojuego
    {
        $this->ventas = $ventas;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return Videojuego
     */
    public function setStock(int $stock): Videojuego
    {
        $this->stock = $stock;
        return $this;
    }

    public function getPathVideojuego()
    {
        return self::RUTA_VIDEOJUEGO;
    }

    public function toArray(): array
    {
        return [
            'nombre' => $this->getNombre(),
            'descripcion' => $this->getDescripcion(),
            'imagen' => $this->getImagen(),
            'precio' => $this->getPrecio(),
            'fechaCreacion' => $this->getFechaCreacion()->format('Y-m-d H:i:s'),
            'plataforma' => $this->getPlataforma(),
            'usuario' => $this->getUsuario(),
            'ventas' => $this->getVentas(),
            'stock' => $this->getStock()
        ];
    }
}