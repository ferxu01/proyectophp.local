<?php
namespace PROYECTOPHP\app\helpers;


class FlashMessage
{
    public static function get(string $clave)
    {
        if (isset($_SESSION[$clave])) {
            $valor = $_SESSION[$clave];
            $_SESSION[$clave] = null;
            unset($_SESSION[$clave]);
        } else {
            $valor = null;
        }

        return $valor;
    }

    public static function set(string $clave, $valor)
    {
        $_SESSION[$clave] = $valor;
    }
}