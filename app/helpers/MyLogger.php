<?php
namespace PROYECTOPHP\app\helpers;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLogger
{
    const LOG_FILE_PATH = __DIR__ . '/../../logs/log.log';

    public static function createLog(string $mensaje)
    {
        $log = new Logger('name');
        $log->pushHandler(
            new StreamHandler(
                self::LOG_FILE_PATH
            )
        );

        $log->info($mensaje);
    }
}