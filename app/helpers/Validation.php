<?php
namespace PROYECTOPHP\app\helpers;

class Validation
{
    public static function checkCamposRegistro(string $nombre, string $apellidos, string $email, string $provincia,
                                               string $username, string $password, string $password2, $imagen, string $captcha): bool
    {
        return ($nombre !== null && $apellidos !== null && $email !== null && $provincia !== null
            && $username !== null && $password !== null && $password2 !== null && $imagen !== '' && $captcha !== '');
    }

    public static function checkPasswordsIguales(string $password, string $password2): bool
    {
        return $password === $password2;
    }

    public static function checkCamposVideojuego(string $nombre, string $descripcion,
                                                      $plataforma, $precio, $imagen): bool
    {
        return ($nombre !== null && $descripcion !== null && $plataforma !== null && (($precio !== null) && (!empty($precio))) && $imagen !== '');
    }
}