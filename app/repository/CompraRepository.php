<?php
namespace PROYECTOPHP\app\repository;

use PROYECTOPHP\app\entity\Comprar;
use PROYECTOPHP\core\database\QueryBuilder;

class CompraRepository extends QueryBuilder
{
    /**
     * CompraRepository constructor.
     */
    public function __construct()
    {
        parent::__construct('comprar', Comprar::class);
    }
}