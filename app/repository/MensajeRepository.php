<?php
namespace PROYECTOPHP\app\repository;

use PROYECTOPHP\app\entity\Mensaje;
use PROYECTOPHP\core\database\QueryBuilder;

class MensajeRepository extends QueryBuilder
{
    /**
     * MensajeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct('mensajes', Mensaje::class);
    }
}