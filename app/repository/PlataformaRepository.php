<?php
namespace PROYECTOPHP\app\repository;

use PROYECTOPHP\core\database\QueryBuilder;
use PROYECTOPHP\app\entity\Plataforma;

class PlataformaRepository extends QueryBuilder
{

    /**
     * PlataformaRepository constructor.
     */
    public function __construct()
    {
        parent::__construct('plataformas', Plataforma::class);
    }
}