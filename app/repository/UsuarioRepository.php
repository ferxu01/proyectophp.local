<?php
namespace PROYECTOPHP\app\repository;

use PROYECTOPHP\core\database\QueryBuilder;
use PROYECTOPHP\app\entity\Usuario;

class UsuarioRepository extends QueryBuilder
{

    /**
     * UsuarioRepository constructor.
     */
    public function __construct()
    {
        parent::__construct('usuarios', Usuario::class);
    }
}