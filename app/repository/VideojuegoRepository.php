<?php
namespace PROYECTOPHP\app\repository;

use PROYECTOPHP\app\entity\Usuario;
use PROYECTOPHP\core\database\QueryBuilder;
use PROYECTOPHP\app\entity\Plataforma;
use PROYECTOPHP\app\entity\Videojuego;

class VideojuegoRepository extends QueryBuilder
{

    /**
     * VideojuegoRepository constructor.
     */
    public function __construct()
    {
        parent::__construct('videojuegos', Videojuego::class);
    }

    public function getPlataforma(Videojuego $videojuego): ?Plataforma
    {
        $plataformaRepository = new PlataformaRepository();
        return $plataformaRepository->find($videojuego->getPlataforma());
    }

    public function getUsuario(Videojuego $videojuego): ?Usuario
    {
        $usuarioRepository = new UsuarioRepository();
        return $usuarioRepository->find($videojuego->getUsuario());
    }

    public static function formatPlataforma(string $plataforma)
    {
        return strtolower($plataforma);
    }

    public function createSlug(string $nombreVideojuego, $max=30)
    {
        $out = iconv('UTF-8', 'ASCII//TRANSLIT', $nombreVideojuego);
        $out = substr(preg_replace("/[^-\/+|\w ]/", '', $out), 0, $max);
        $out = strtolower(trim($out, '-'));
        $out = preg_replace("/[\/_| -]+/", '-', $out);

        if ($this->checkSlug($out))
            return $out;

        return null;
    }

    private function checkSlug(string $nombreVideojuego)
    {
        return preg_match("/^[a-z0-9\-]*$/", $nombreVideojuego);
    }
}