<?php
use PROYECTOPHP\app\controllers\AuthController;
use PROYECTOPHP\app\controllers\ComprarController;
use PROYECTOPHP\app\controllers\IdiomaController;
use PROYECTOPHP\app\controllers\ImagenController;
use PROYECTOPHP\app\controllers\MensajeController;
use PROYECTOPHP\app\controllers\PagesController;
use PROYECTOPHP\app\controllers\VideojuegoController;
use PROYECTOPHP\app\controllers\UsuarioController;
use PROYECTOPHP\core\App;

$router = App::get('router');

$router->get('idioma/:idioma', IdiomaController::class, 'traduce');
$router->get('', PagesController::class, 'index');
$router->get('categoria', VideojuegoController::class, 'categoria');
$router->get('sobre-nosotros', PagesController::class, 'sobreNosotros');
$router->get('contacto', PagesController::class, 'contacto', 'ROL_USUARIO');
$router->get('videojuegos/nuevo', VideojuegoController::class, 'formNuevoVideojuego', 'ROL_USUARIO');
$router->post('videojuegos/nuevo', VideojuegoController::class, 'nuevoVideojuego', 'ROL_USUARIO');
$router->get('videojuegos/:plataforma/:nombre/:id', VideojuegoController::class, 'detallesVideojuego');
$router->get('mi-perfil', UsuarioController::class, 'miPerfil', 'ROL_USUARIO');
$router->get('mis-videojuegos', VideojuegoController::class, 'misVideojuegos', 'ROL_USUARIO');
$router->delete('videojuegos/borrar/:id', VideojuegoController::class, 'borrarJson', 'ROL_USUARIO');
$router->get('videojuegos/editar/:id', VideojuegoController::class, 'formEditarVideojuego', 'ROL_USUARIO');
$router->post('videojuegos/editar/:id', VideojuegoController::class, 'editarVideojuego', 'ROL_USUARIO');
$router->get('registro', AuthController::class, 'registrar');
$router->post('usuarios/nuevo', AuthController::class, 'nuevoUsuario');
$router->get('login', AuthController::class, 'login');
$router->post('login', AuthController::class, 'checkLogin');
$router->get('logout', AuthController::class, 'logout', 'ROL_USUARIO');
$router->get('usuarios/gestion', UsuarioController::class, 'gestionUsuarios', 'ROL_ADMIN');
$router->post('usuarios/editar/rol/:id', UsuarioController::class, 'editarRol', 'ROL_ADMIN');
$router->post('usuarios/filtrarRol', UsuarioController::class, 'filtrarRol', 'ROL_ADMIN');

$router->post('videojuegos/filtrar/:pagina', VideojuegoController::class, 'filtrarVideojuegos');

$router->get('videojuegos/:plataforma', PagesController::class, 'videojuegosPorPlataforma');

$router->delete('usuario/borrar/:id', UsuarioController::class, 'borrarUsuario', 'ROL_ADMIN');
$router->get('usuario/editar/password', UsuarioController::class, 'formEditarPassword', 'ROL_USUARIO');
$router->post('usuario/editar/password', UsuarioController::class, 'editarPassword', 'ROL_USUARIO');
$router->get('usuario/editar/avatar', UsuarioController::class, 'formEditarAvatar', 'ROL_USUARIO');
$router->post('usuario/editar/avatar', UsuarioController::class, 'editarAvatar', 'ROL_USUARIO');

$router->get('user/:id/videojuegos', PagesController::class, 'mostrarVideojuegosUsuario');
$router->post('videojuegos/buscar', VideojuegoController::class, 'buscar');

$router->get('usuario/carro', ComprarController::class, 'carroCompra', 'ROL_USUARIO');
$router->get('videojuego/nuevo/carro/:id', ComprarController::class, 'addVideojuegoACarro', 'ROL_USUARIO');
$router->delete('videojuego/borrar/carro/:id', ComprarController::class, 'eliminarVideojuegoCarro', 'ROL_USUARIO'); //Pendiente
$router->post('videojuego/comprar', ComprarController::class, 'comprarVideojuegos', 'ROL_USUARIO');

$router->get('mis-mensajes', MensajeController::class, 'mostrarMensajes', 'ROL_USUARIO');
$router->get('mensaje/nuevo', MensajeController::class, 'nuevoMensaje', 'ROL_USUARIO');
$router->get('mensaje/nuevo/:id', MensajeController::class, 'nuevoMensaje', 'ROL_USUARIO');
$router->post('mensaje/enviar', MensajeController::class, 'enviarMensaje', 'ROL_USUARIO');
$router->get('mensajes/responder/:id', MensajeController::class, 'responderMensaje', 'ROL_USUARIO');
$router->post('mensajes/respuesta/enviar/:id', MensajeController::class, 'enviarRespuesta', 'ROL_USUARIO');
$router->delete('mensaje/borrar/:id', MensajeController::class, 'borrarMensaje', 'ROL_USUARIO');

$router->get('generarCaptcha', AuthController::class, 'generarCaptcha');
$router->get('generaImagen/:tipo/:id', ImagenController::class, 'generarImagenAvatar');
$router->get('generaImagen/mini/:tipo/:id', ImagenController::class, 'generarMiniMiniaturaAvatar');
$router->get('generaImagen/mini/:tipo/:plataforma/:id', ImagenController::class, 'generarMiniaturaVideojuegoCarro');
$router->get('generaImagen/:tipo/:plataforma/:id', ImagenController::class, 'generarImagenVideojuego');
