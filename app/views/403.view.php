<div class="container">
    <h2><?= _('403 - Acceso no autorizado') ?></h2>
    <p><?= _('No tienes suficientes privilegios para acceder a esta sección') ?></p>
    <div style="height: 300px"></div>
</div>