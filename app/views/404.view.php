<div class="container">
    <h2><?= _('404 - Página no encontrada') ?></h2>
    <p><?= _('No se ha encontrado la página indicada') ?></p>
    <div style="height: 300px"></div>
</div>