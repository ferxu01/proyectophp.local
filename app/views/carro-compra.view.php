<!--================Cart Area =================-->
<section class="cart_area">
    <div class="container">
        <div class="cart_inner">
            <div class="table-responsive">
                <h2><?= _('Carro de compra') ?></h2>
                <?php include 'partials/success.part.php'; ?>
                <?php if (!is_null($videojuegos)) : ?>
                <table class="table mt-30">
                    <thead>

                    <tr align="center">
                        <th scope="col"><?= _('Videojuego') ?></th>
                        <th scope="col"><?= _('Precio') ?></th>
                        <th scope="col"><?= _('Cantidad') ?></th>
                        <th scope="col"><?= _('Total') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($videojuegos as $key => $videojuego) : ?>
                    <tr align="center">
                        <td>
                            <div class="media">
                                <div class="d-flex">
                                    <img src="/generaImagen/mini/videojuego/<?= strtoupper($videojuegoRepository->createSlug(
                                            $videojuegoRepository->getPlataforma($videojuego)->getNombre())) ?>/<?= $videojuego->getId() ?>" alt="Miniatura videojuego carro">
                                </div>
                                <div class="media-body">
                                    <p><?= $videojuego->getNombre() ?></p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p><?= $videojuego->getPrecio() ?>€</p>
                        </td>
                        <td>
                            <span><?= $numArticulos[$videojuego->getId()] ?></span>
                        </td>
                        <td>
                            <p><?= $videojuego->getPrecio()*$numArticulos[$videojuego->getId()] ?>€</p>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <p><?= _('Subtotal') ?></p>
                        </td>
                        <td>
                            <p><?= $acumuladoPrecioCarro ?>€</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="checkout_btn_inner float-right">
                    <form action="/videojuego/comprar" method="post">
                        <input type="submit" id="btnComprar" class="btn_1" value="<?= _('Comprar') ?>">
                    </form>
                </div>
                <?php else: ?>
                <p><?= _('No tienes artículos en el carro') ?></p>
                <?php endif; ?>
            </div>
        </div>
        <script src="/assets/js/videojuegos.js"></script>
</section>
<!--================End Cart Area =================-->