    <!-- ================ contact section start ================= -->
    <section>
        <div class="container">
            <div class="d-none d-sm-block mb-5 pb-4">
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title"><?= _('Contacta con nosotros') ?></h2>
                </div>
                <div class="col-lg-8">
                    <form class="form-contact" action="/enviarCorreo" method="post" id="contactForm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input placeholder="<?= _('Nombre') ?>" class="form-control" name="nombre" id="nombre" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Nombre") ?>'">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input placeholder="<?= _('Correo electrónico') ?>" class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Correo electrónico") ?>'">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input placeholder="<?= _('Asunto') ?>" class="form-control" name="asunto" id="asunto" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Asunto") ?>'">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea placeholder="<?= _('Mensaje') ?>" class="form-control w-100" name="mensaje" id="mensaje" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Mensaje") ?>'" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="submit" value="<?= _('Enviar') ?>" class="button button-contactForm">
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body">
                            <h3><?= _('Av/ Aguilera, Alicante') ?></h3>
                            <p><?= _('CP: 03006') ?></p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body">
                            <h3>+34 965129374</h3>
                            <p><?= _('Lunes a Viernes de 9h a 17h') ?></p>
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            <h3>support@ferplay.com</h3>
                            <p><?= _('¡Envíanos tus preguntas!') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ contact section end ================= -->