<div class="container">
    <div class="align-content-around">
        <h2><?= _('Cambiar avatar') ?></h2>
        <form class="mt-50 form-group" action="/usuario/editar/avatar" method="post" enctype="multipart/form-data">
            <?php include 'partials/error.part.php'; ?>
            <?php include 'partials/success.part.php'; ?>
            <label for="editarAvatar"><?= _('Nuevo avatar') ?></label>
            <div class="form-control-file mb-10">
                <input type="file" name="avatar">
            </div>
            <input class="btn-sm btn-primary" type="submit" value="<?= _('Enviar') ?>">
        </form>
    </div>
</div>