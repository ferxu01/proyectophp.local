<div class="container">
    <div class="align-content-around">
        <h2><?= _('Cambiar contraseña') ?></h2>
        <form class="mt-50 form-group" action="/usuario/editar/password" method="post">
            <?php include 'partials/error.part.php'; ?>
            <?php include 'partials/success.part.php'; ?>
            <div class="form-text">
                <label for="passwordActual"><?= _('Contraseña actual') ?></label>
                <input type="password" name="passwordActual">
            </div>
            <div class="form-text">
                <label for="nuevaPassword"><?= _('Nueva contraseña') ?></label>
                <input type="password" name="nuevaPassword">
            </div>
            <input class="btn-sm btn-primary" type="submit" value="<?= _('Enviar') ?>">
        </form>
    </div>
</div>