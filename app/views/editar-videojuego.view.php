<div class="container">
    <div class="row justify-content-center align-content-center">
        <div class="col-md-12">
            <h2 class="text-center">
                <?= _('Editar videojuego') ?>
            </h2>
        </div>
        <div class="col-md-6">
            <form action="/videojuegos/editar/<?= $videojuego->getId() ?>" method="post" enctype="multipart/form-data">
                <?php include 'partials/error.part.php'; ?>
                <?php include 'partials/success.part.php'; ?>
                <div class="form-group">
                    <label for="nombre"><?= _('Nombre') ?></label>
                    <input type="text" class="form-control" id="nombre" name="nuevoNombre" value="<?= $videojuego->getNombre() ?>">
                </div>
                <div class="form-group">
                    <label for="descripcion"><?= _('Descripción') ?></label>
                    <textarea class="form-control" id="descripcion" name="nuevaDescripcion" rows="5" cols="5"><?= $videojuego->getDescripcion() ?></textarea>
                </div>
                <div class="form-group">
                    <label for="plataforma"><?= _('Plataforma') ?></label>
                    <select class="form-select" id="plataforma" name="nuevaPlataforma">
                        <option value="0"><?= _('Seleccionar') ?></option>
                        <?php foreach ($plataformas as $plataforma) : ?>
                            <option <?= ($videojuego->getPlataforma() === $plataforma->getId()) ? 'selected' : '' ?> value="<?= $plataforma->getId() ?>"><?= $plataforma->getNombre() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group mt-70">
                    <label for="precio"><?= _('Precio') ?></label>
                    <input type="number" id="precio" name="nuevoPrecio" min="1" max="100" value="<?= $videojuego->getPrecio() ?>">
                </div>
                <div class="form-group mt-30">
                    <label for="imagen"><?= _('Imagen') ?></label>
                    <input type="file" id="imagen" name="nuevaImagen">
                </div>
                <input type="submit" class="btn btn-primary" value="<?= _('Enviar') ?>">
            </form>
        </div>
    </div>
</div>