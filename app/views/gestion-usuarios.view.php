<div class="container">
    <div>
        <form action="/usuarios/filtrarRol" method="post">
            <select onchange="this.form.submit()" name="rolUsuario" id="rolUsuario">
                <option <?= (isset($_POST['rolUsuario']) && $_POST['rolUsuario'] === 'ROl_ADMIN') ? 'selected' : '' ?> value="ROL_ADMIN"><?= _('ADMIN') ?></option>
                <option <?= (isset($_POST['rolUsuario']) && $_POST['rolUsuario'] === 'ROL_USUARIO') ? 'selected' : '' ?> value="ROL_USUARIO"><?= _('USUARIO') ?></option>
            </select>
        </form>
    </div>
    <table class="table table-hover">
        <thead style="text-align: center">
            <th><?= _('ID') ?></th>
            <th><?= _('Nombre') ?></th>
            <th><?= _('Apellidos') ?></th>
            <th><?= _('Correo electrónico') ?></th>
            <th><?= _('Provincia') ?></th>
            <th><?= _('Avatar') ?></th>
            <th><?= _('Nombre usuario') ?></th>
            <th><?= _('Fecha creación') ?></th>
            <th><?= _('Cambiar rol') ?></th>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario) : ?>
                <tr style="text-align: center">
                    <td><?= $usuario->getId() ?></td>
                    <td><?= $usuario->getNombre() ?></td>
                    <td><?= $usuario->getApellidos() ?></td>
                    <td><?= $usuario->getEmail() ?></td>
                    <td><?= $usuario->getProvincia() ?></td>
                    <td><img src="/generaImagen/mini/avatar/<?= $usuario->getId() ?>" alt="Foto perfil"></td>
                    <td><?= $usuario->getUsuario() ?></td>
                    <td style="width: 200px"><?= $usuario->getFechaAlta()->format('d-m-Y') ?></td>
                    <td>
                        <form action="/usuarios/editar/rol/<?= $usuario->getId() ?>" method="post">
                            <select onchange="this.form.submit()" name="rolUsuario" id="rolUsuario">
                                <?php if ($usuario->getRol() === 'ROL_ADMIN') : ?>
                                    <option selected value="ROL_ADMIN"><?= _('ADMIN') ?></option>
                                    <option value="ROL_USUARIO"><?= _('USUARIO') ?></option>
                                <?php else : ?>
                                    <option value="ROL_ADMIN"><?= _('ADMIN') ?></option>
                                    <option selected value="ROL_USUARIO"><?= _('USUARIO') ?></option>
                                <?php endif; ?>
                            </select>
                        </form>
                    </td>
                    <td>
                        <a id="btnBorrarJson" href="/usuario/borrar/<?= $usuario->getId() ?>" class="btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>