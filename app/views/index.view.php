<!-- Latest Products Start -->
<section class="latest-product-area">
    <div class="container mt-50">
        <div class="row product-btn d-flex justify-content-end align-items-end">
            <!-- Section Tittle -->
            <div class="col-xl-4 col-lg-5 col-md-5">
                <div class="section-tittle mb-30">
                    <h2><?= _('Más recientes') ?></h2>
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-7">
                <div class="f-right">
                    <form action="/videojuegos/filtrar/index" method="post">
                        <div class="form-group">
                            <select class="form-control" id="plataformaFiltrada" name="plataformaFiltrada">
                                <option value="0"><?= _('Plataforma') ?></option>
                                <?php foreach ($plataformas as $plataforma) : ?>
                                    <option value="<?= $plataforma->getId() ?>"><?= $plataforma->getNombre() ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input class="btn-sm btn-dark" type="submit" value="<?= _('Filtrar') ?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Nav Card -->
        <div class="tab-content" id="nav-tabContent">
            <!-- Sección videojuegos recientes -->
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div id="datosJsonBuscar" class="row">
                    <?php
                    foreach ($videojuegos as $videojuego)
                        include 'partials/videojuego.part.php';
                    ?>
                </div>
            </div>
            <?php include 'partials/paginacion.part.php'; ?>
        </div>
        <!-- End Nav Card -->
    </div>
</section>
<!-- Latest Products End -->