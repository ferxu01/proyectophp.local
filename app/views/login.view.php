<!--================login_part Area =================-->
<section class="login_part mt-70">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="login_part_text text-center">
                    <div class="login_part_text_iner">
                        <h2><?= _('¿Nuevo en nuestra tienda?') ?></h2>
                        <p><?= _('Con tu cuenta podrás acceder a las diferentes funcionalidades de nuestra tienda, entre ellas
                            está la de publicar productos y comprarlos') ?>.</p>
                        <a href="/registro" class="btn_3"><?= _('Crear una cuenta') ?></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="login_part_form">
                    <div class="login_part_form_iner">
                        <h3><?= _('Bienvenido') ?> <br>
                            <?= _('Regístrate ahora') ?></h3>
                        <?php include __DIR__ . '/partials/error.part.php'; ?>
                        <form class="row contact_form" action="/login" method="post" novalidate="novalidate">
                            <div class="col-md-12 form-group p_star">
                                <input type="email" class="form-control" id="email" name="email" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>"
                                       placeholder="<?= _('Correo electrónico') ?>">
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <input type="password" class="form-control" id="password" name="password" value=""
                                       placeholder="<?= _('Contraseña') ?>">
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="creat_account d-flex align-items-center">
                                    <input type="checkbox" id="f-option" name="selector">
                                    <label for="f-option"><?= _('Recuérdame') ?></label>
                                </div>
                                <button type="submit" value="submit" class="btn_3">
                                    <?= _('Acceder') ?>
                                </button>
                                <a class="lost_pass" href="#"><?= _('¿Olvidaste la contraseña?') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================login_part end =================-->