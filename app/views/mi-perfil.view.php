<div class="container">
    <h1><?= _('Mi perfil') ?></h1>
    <div class="mt-30"></div>
    <div class="card mb-3" style="max-width: 900px">
        <div class="row no-gutters">
            <div class="col-lg-5">
                <img src="/generaImagen/avatar/<?= $usuario->getId() ?>" alt="Miniatura">
            </div>
            <div class="col-lg-7">
                <div class="card-body">
                    <h3 class="card-title">
                        <?= $usuario->getNombre() . ' ' . $usuario->getApellidos() ?>
                    </h3>
                    <p class="card-text"><?= $usuario->getEmail() ?></p>
                    <p class="card-text"><?= _('Provincia') ?>: <?= $usuario->getProvincia() ?></p>
                    <p class="card-text"><?= _('Nombre usuario') ?>: <?= $usuario->getUsuario() ?></p>
                    <p class="card-text"><?= _('Fecha creación') ?>: <?= $usuario->getFechaAlta()->format('d-m-Y') ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row align-items-center justify-content-center">
        <a href="/usuario/editar/password" class="btn btn-primary"><?= _('Cambiar contraseña') ?></a>
        <a href="/usuario/editar/avatar" class="btn btn-primary ml-30"><?= _('Cambiar avatar') ?></a>
    </div>
</div>