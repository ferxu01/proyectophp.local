<div class="container">
    <h2 class="mb-20"><?= _('Mis mensajes') ?></h2>
    <?php if (!empty($mensajes) && isset($correoEmisor)) : ?>
        <?php foreach ($mensajes as $key => $mensaje) : ?>
            <?php include 'partials/mensaje.part.php'; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <br>
        <p><?= _('No tienes mensajes') ?></p>
    <?php endif; ?>
    <?php
        if (isset($success))
            include 'partials/success.part.php';
    ?>
    <a class="btn btn-primary mt-20" href="/mensaje/nuevo"><?= _('Nuevo mensaje') ?></a>
</div>
