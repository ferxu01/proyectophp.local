<?php
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;

$repoVideojuego = App::getRepository(VideojuegoRepository::class);
?>

<div class="container mt-25">
    <h2><?= _('Mis videojuegos') ?></h2>
    <?php if (!empty($videojuegos)) : ?>
    <div class="f-right">
        <form action="/videojuegos/filtrar/mis-videojuegos" method="post">
            <div class="form-group">
                <select class="form-control" id="plataformaFiltrada" name="plataformaFiltrada">
                    <option value="0"><?= _('Plataforma') ?></option>
                    <?php foreach ($plataformas as $plataforma) : ?>
                        <option value="<?= $plataforma->getId() ?>"><?= $plataforma->getNombre() ?></option>
                    <?php endforeach; ?>
                </select>
                <input class="btn-sm btn-dark" type="submit" value="<?= _('Filtrar') ?>">
            </div>
        </form>
    </div>
    <table class="table table-hover mt-15">
        <thead>
        <tr style="text-align: center">
            <th scope="col"><?= _('Imagen') ?></th>
            <th scope="col"><?= _('Nombre') ?></th>
            <th scope="col"><?= _('Descripción') ?></th>
            <th scope="col"><?= _('Plataforma') ?></th>
            <th scope="col"><?= _('Precio') ?></th>
            <th scope="col"><?= _('Fecha creación') ?></th>
            <th scope="col"><?= _('Opciones') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($videojuegos as $videojuego) : ?>
        <tr style="text-align: center">
            <td>
                <img src="/generaImagen/mini/videojuego/<?= strtoupper($repoVideojuego->createSlug($repoVideojuego->getPlataforma($videojuego)->getNombre())) ?>/<?= $videojuego->getId() ?>">
            </td>
            <td><?= $videojuego->getNombre() ?></td>
            <td style="width: 300px; text-align: left"><?= $videojuego->getDescripcion() ?></td>
            <td><?= App::getRepository(VideojuegoRepository::class)->getPlataforma($videojuego)->getNombre() ?></td>
            <td><?= $videojuego->getPrecio() ?>€</td>
            <td><?= $videojuego->getFechaCreacion()->format('d-m-Y') ?></td>
            <td>
                <a href="/videojuegos/editar/<?= $videojuego->getId() ?>" class="btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                <a id="btnBorrarJson" class="btn-sm btn-danger" href="/videojuegos/borrar/<?= $videojuego->getId() ?>"><i class="fa fa-trash"></i></a>
                <a href="/videojuegos/<?= $repoVideojuego->createSlug($repoVideojuego->getPlataforma($videojuego)->getNombre())
                . '/' . $repoVideojuego->createSlug($videojuego->getNombre())
                . '/' . $videojuego->getId() ?>" class="btn-sm btn-secondary"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
        <br>
        <p><?= _('No tienes videojuego creados') ?></p>
    <?php endif; ?>
    <div class="row justify-content-end">
        <a href="/videojuegos/nuevo" class="btn btn-secondary"><?= _('Crear videojuego') ?></a>
    </div>
</div>
<script src="/assets/js/videojuegos.js"></script>
