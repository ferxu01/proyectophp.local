<section>
    <div class="container">
        <div class="d-none d-sm-block mb-5 pb-4">
            <div class="row">
                <div class="col-12">
                    <h2 class="contact-title"><?= _('Enviar un mensaje') ?></h2>
                </div>
                <div class="col-lg-8">
                    <form class="form-contact" action="/mensaje/enviar" method="post" id="messageForm">
                        <?php
                        if (isset($error))
                            include 'partials/error.part.php';
                        ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input <?= isset($usuarioReceptor) ? 'readonly' : '' ?> value="<?= isset($usuarioReceptor) ? $usuarioReceptor : '' ?>" placeholder="<?= _('Destinatario') ?>" class="form-control" name="destinatario" id="destinatario" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Destinatario") ?>'">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input placeholder="<?= _('Asunto') ?>" class="form-control" name="asunto" id="asunto" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Asunto") ?>'">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea placeholder="<?= _('Mensaje') ?>" class="form-control w-100" name="textoMensaje" id="textoMensaje" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?= _("Mensaje") ?>'" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="submit" value="<?= _('Enviar') ?>" class="btn btn-primary">
                        </div>
                    </form>
                </div>
        </div>
</section>