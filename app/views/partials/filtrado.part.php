<div class="f-right">
    <form action="/videojuegos/filtrar/" method="post">
        <div class="form-group">
            <select class="form-control" id="plataformaFiltrada" name="plataformaFiltrada">
                <option value="0"><?= _('Plataforma') ?></option>
                <?php foreach ($plataformas as $plataforma) : ?>
                    <option value="<?= $plataforma->getId() ?>"><?= $plataforma->getNombre() ?></option>
                <?php endforeach; ?>
            </select>
            <label for="fechaFiltradoDesde"><?= _('Desde') ?>:</label>
            <input type="date" name="fechaFiltradoDesde">
            <label for="fechaFiltradoHasta"><?= _('Hasta') ?>:</label>
            <input type="date" name="fechaFiltradoHasta">
            <input type="submit" value="<?= _('Filtrar') ?>">
        </div>
    </form>
</div>