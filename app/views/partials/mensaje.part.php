<div class="card mt-20">
    <div class="card-header"><?= _('De') . ': ' . $correoEmisor[$key] ?></div>
    <div class="card-body">
        <h5 class="card-title"><?= $mensaje->getAsunto() ?></h5>
        <p class="card-text"><?= $mensaje->getTexto() ?></p>
        <a class="btn-sm btn-dark" href="/mensajes/responder/<?= $mensaje->getId() ?>"><?= _('Responder') ?></a>
    </div>
</div>