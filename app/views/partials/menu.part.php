<?php
use PROYECTOPHP\app\repository\PlataformaRepository;
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;

$repoVideojuego = App::getRepository(VideojuegoRepository::class);
$plataformas = App::getRepository(PlataformaRepository::class)->findAll();
?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ferplay</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="manifest" href="site.webmanifest">-->
    <link rel="shortcut icon" type="image/x-icon" href="/assets/img/icon/controller-icon.png">

    <!-- CSS here -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/flaticon.css">
    <link rel="stylesheet" href="/assets/css/slicknav.css">
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <link rel="stylesheet" href="/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/assets/css/themify-icons.css">
    <link rel="stylesheet" href="/assets/css/slick.css">
    <link rel="stylesheet" href="/assets/css/nice-select.css">
    <link rel="stylesheet" href="/assets/css/style.css">
</head>

<body>

<!-- Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="/assets/img/logo/logo.png" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Preloader Start -->

<header>
    <!-- Header Start -->
    <div class="header-area">
        <div class="main-header ">
            <?php if (!is_null(App::get('user'))) : ?>
            <div class="header-top top-bg d-none d-lg-block">
                <div class="container-fluid">
                    <div class="col-xl-12">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="header-info-left d-flex">
                                <div class="select-this">
                                    <!-- Cambiar el idioma de la página -->
                                    <div class="dropdown rounded">
                                        <button class="btn-sm btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php if($_SESSION['idioma'] === 'es_ES')
                                                    echo _('Español')  ?>

                                            <?php if($_SESSION['idioma'] === 'en_GB')
                                                    echo _('Inglés') ?>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/idioma/es_ES"><img src="/assets/img/icon/spain_flag.png" alt="ES"> <?= _('ES') ?> </a>
                                            <a class="dropdown-item" href="/idioma/en_GB"><img src="/assets/img/icon/english_flag.png" alt="EN"> <?= _('EN') ?> </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="header-info-right">
                                <ul>
                                    <?php if ($_usuario->getRol() === 'ROL_ADMIN') : ?>
                                        <li><a href="/usuarios/gestion"><?= _('Gestión de usuarios') ?></a></li>
                                    <?php endif; ?>
                                    <li><a href="/mi-perfil"><?= _('Mi perfil') ?></a></li>
                                    <li><a href="/mis-videojuegos"><?= _('Mis videojuegos') ?></a></li>
                                    <li><a href="/mis-mensajes"><?= _('Mis mensajes') ?></a></li>
                                    <li><a href="/logout"><?= _('Cerrar sesión') ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="header-bottom  header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-3">
                            <div class="logo">
                                <a href="/"><h2 class="font-weight-bold" style="font-family: Helvetica, sans-serif">FERPLAY</h2></a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8 col-md-7 col-sm-5">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a href="/"><?= _('Inicio') ?></a></li>
                                        <li><a href="/categoria"><?= _('Categoría') ?></a></li>
                                        <li><a href="#"><?= _('Videojuegos') ?></a>
                                            <ul class="submenu">
                                                <?php foreach ($plataformas as $plataforma) : ?>
                                                <li>
                                                    <a href="/videojuegos/<?= $repoVideojuego->createSlug($plataforma->getNombre()) ?>">
                                                        <?= $plataforma->getNombre() ?>
                                                    </a>
                                                </li>
                                                <? endforeach; ?>
                                            </ul>
                                        </li>

                                        <li><a href="/sobre-nosotros"><?= _('Sobre nosotros') ?></a></li>
                                        <li><a href="/contacto"><?= _('Contacto') ?></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-3 col-md-3 col-sm-3 fix-card">
                            <ul class="header-right f-right d-none d-lg-block d-flex justify-content-between">
                                <li class="d-none d-xl-block">
                                    <div class="form-box f-right ">
                                        <input id="searchInput" type="text" name="busqueda" placeholder="<?= _('Buscar productos') ?>">
                                        <div class="search-icon">
                                            <i id="btnSearch" class="fas fa-search special-tag"></i>
                                        </div>
                                    </div>
                                </li>
                                <!-- Funcionalidad opcional
                                <li class=" d-none d-xl-block">
                                    <div class="favorit-items">
                                        <i class="far fa-heart"></i>
                                    </div>
                                </li>-->
                                <li>
                                    <div class="shopping-card">
                                        <a href="/usuario/carro"><i class="fas fa-shopping-cart"></i></a>
                                    </div>
                                </li>
                                <?php
                                if (is_null($_usuario)) : ?>
                                <li class="d-none d-lg-block"> <a href="/login" class="btn header-btn"><?= _('Iniciar sesión') ?></a></li>
                                <?php else: ?>
                                    <li class="d-none d-lg-block"><?= $_usuario->getUsuario() ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>

<!-- Banner de categoría individual de videojuegos -->
<?php

if (isset($headerSlider) && $headerSlider->isVideojuego())
    include __DIR__ . '/header-slider-area.part.php';

?>

<main>