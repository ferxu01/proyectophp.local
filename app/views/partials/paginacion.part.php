<?php
if (!isset($_GET['pag']))
    $_GET['pag'] = 1;
?>

<!-- Paginación videojuegos -->
<?php if (isset($totalPaginas)) : ?>
<nav class="mt-50" aria-label="Paginacion">
    <ul class="pagination justify-content-center">
        <li class="page-item <?= $_GET['pag'] <= 1 ? 'disabled' : '' ?>">
            <a href="?pag=<?= $_GET['pag']-1 ?>" class="page-link"><?= _('Anterior') ?></a>
        </li>

        <?php for ($i=0; $i<$totalPaginas; $i++) : ?>
            <li class="page-item <?= $_GET['pag'] == $i+1 ? 'active' : '' ?>">
                <a href="?pag=<?= $i+1 ?>" class="page-link"><?= $i+1 ?></a>
            </li>
        <?php endfor; ?>

        <li class="page-item <?= $_GET['pag'] >= $totalPaginas ? 'disabled' : '' ?>">
            <a href="?pag=<?= $_GET['pag']+1 ?>" class="page-link"><?= _('Siguiente') ?></a>
        </li>
    </ul>
</nav>
<?php endif; ?>