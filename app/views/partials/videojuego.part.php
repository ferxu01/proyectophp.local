<?php
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;

$repoVideojuego = App::getRepository(VideojuegoRepository::class);
?>

<!-- Partial de videojuegos del index -->
<div class="card">
    <img src="/generaImagen/videojuego/<?= strtoupper($repoVideojuego->createSlug($repoVideojuego->getPlataforma($videojuego)->getNombre())) ?>/<?= $videojuego->getId() ?>" alt="Miniatura videojuego">
    <div class="card-body">
        <a href="/videojuegos/<?= $repoVideojuego->createSlug($repoVideojuego->getPlataforma($videojuego)->getNombre())
        . '/' . $repoVideojuego->createSlug($videojuego->getNombre())
        . '/' . $videojuego->getId() ?>"><h5><?= $videojuego->getNombre() ?></h5></a>
        
        <p class="card-text"><?= $videojuego->getFechaCreacion()->format('d-m-Y') ?></p>
    </div>
    <div class="row justify-content-around">
        <p class="card-text"><?= App::getRepository(VideojuegoRepository::class)->getPlataforma($videojuego)->getNombre() ?></p>
        <p class="card-text"><?= App::getRepository(VideojuegoRepository::class)->getUsuario($videojuego)->getUsuario() ?></p>
    </div>
    <div class="card-body">
        <div class="row justify-content-around">
            <p class="card-text text-left"><?= $videojuego->getPrecio() ?> €</p>
            <div class="btn-group-sm">
            <?php if (isset($_usuario) && ($_usuario->getId() === $videojuego->getUsuario()) || isset($_usuario) && $_usuario->getRol() === 'ROL_ADMIN') : ?>
                <a href="/videojuegos/editar/<?= $videojuego->getId() ?>"><button class="btn-sm btn-primary"><?= _('Editar') ?></button></a>
            <?php endif; ?>
                <a href="/videojuegos/<?= $repoVideojuego->createSlug($repoVideojuego->getPlataforma($videojuego)->getNombre())
                . '/' . $repoVideojuego->createSlug($videojuego->getNombre())
                . '/' . $videojuego->getId() ?>"><button class="btn-sm btn-primary"><?= _('Más info') ?></button></a>
            </div>
        </div>
    </div>
</div>