<div class="container mt-50">
    <div class="row justify-content-center align-content-center">
        <div class="col-md-12">
            <h2 class="text-center">
                <?= _('Registro') ?>
            </h2>
        </div>
        <div class="col-md-6 mt-50">
            <?php include __DIR__ . '/partials/error.part.php'; ?>
            <form action="/usuarios/nuevo" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="nombre"><?= _('Nombre') ?></label>
                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php if(isset($_GET['nombre'])) echo $_GET['nombre'] ?>">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="descripcion"><?= _('Apellidos') ?></label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos" value="<?php if(isset($_GET['apellidos'])) echo $_GET['apellidos'] ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email"><?= _('Email') ?></label>
                    <input type="email" class="form-control" id="email" name="email" value="<?php if(isset($_GET['email'])) echo $_GET['email'] ?>">
                </div>
                <div class="form-group">
                    <label for="provincia"><?= _('Provincia') ?></label>
                    <input type="text" class="form-control" id="provincia" name="provincia" value="<?php if(isset($_GET['provincia'])) echo $_GET['provincia'] ?>">
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="usuario"><?= _('Nombre usuario') ?></label>
                            <input type="text" class="form-control" id="usuario" name="usuario" value="<?php if(isset($_GET['usuario'])) echo $_GET['usuario'] ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="password"><?= _('Contraseña') ?></label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="password2"><?= _('Repite contraseña') ?></label>
                            <input type="password" class="form-control" id="password2" name="password2">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="avatar"><?= _('Avatar') ?></label>
                    <input type="file" class="form-control-file" id="avatar" name="avatar">
                </div>
                <!-- Generación de captcha -->
                <div class="form-group mt-30">
                    <label for="captcha"><?= _('Escribe el texto del Captcha') ?></label>
                    <br>
                    <img class="imagen-captcha" src="/generarCaptcha" alt="captcha"><i id="recargarCaptcha" class="fas fa-redo fa-refresh"></i>
                    <br>
                    <input class="mt-10" type="text" id="captcha" name="captchaChallenge">
                </div>
                <input type="submit" class="btn btn-primary" value="<?= _('Enviar') ?>">
            </form>
        </div>
    </div>
</div>
<script src="/assets/js/captcha.js"></script>