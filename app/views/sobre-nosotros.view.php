<!-- feature part here -->
<section class="feature_part mt-25">
    <div class="container">
        <div class="row justify-content-center">
            <div style="max-width: 700px">
                <h2><?= _('Sobre nosotros') ?></h2>
                <hr>
                <p><?= _('Ferplay es una empresa española situada en Alicante, creada por y para vender videojuegos.') ?></p>
                <p><?= _('Nuestra empresa ha sido creada en 2020 con el propósito de vender videojuegos de todo tipo.') ?></p>
                <p><?= _('Nuestra intención y empeño ha sido siempre elegir y vender todos nuestros juegos con el mayor cuidado
                    posible para ofrecer la mejor calidad a nuestros clientes.') ?></p>
                <p><?= _('Síguenos en Facebook, Instagram y Twitter para ser el primero en enterarte de todas nuestras novedades
                    y mucho más.') ?></p>
            </div>
        </div>
    </div>
</section>
<!-- feature part end -->