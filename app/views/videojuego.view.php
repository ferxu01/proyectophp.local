<?php
use PROYECTOPHP\app\repository\VideojuegoRepository;
use PROYECTOPHP\core\App;

$videojuegoRepository = App::getRepository(VideojuegoRepository::class);
?>
<!--================Single Product Area =================-->
<div class="mt-40">
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 790px">
                <div class="row">
                        <img style="width: 25rem; margin-left: 15px;" src="<?= $videojuego->getPathVideojuego() .
                        strtoupper($videojuegoRepository->createSlug($videojuegoRepository->getPlataforma($videojuego)->getNombre()))
                        .'/'. $videojuego->getImagen() ?>" class="card-img" alt="<?= $videojuego->getImagen() ?>">
                    <div class="card-body" style="max-width: 400px">
                        <h5 class="card-title"><?= $videojuego->getNombre() ?></h5>
                        <p class="card-text"><?= $videojuego->getDescripcion() ?></p>
                        <p class="card-text"><?= $videojuego->getPrecio() ?>€</p>
                        <a class="card-text" href="/user/<?= $videojuego->getUsuario() ?>/videojuegos"><p><?= $videojuegoRepository->getUsuario($videojuego)->getUsuario() ?></p></a>
                        <p class="card-text"><?= _('Creado el') ?> <?= $videojuego->getFechaCreacion()->format('d-m-Y') ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card_area">
            <?php
            if (isset($success))
                include 'partials/success.part.php';
            ?>
            <div class="card-body row justify-content-center mt-30">
                <a href="/videojuego/nuevo/carro/<?= $videojuego->getId() ?>" class="btn_3"><?= _('Añadir a la cesta') ?></a>
                <?php if (isset($_usuario) && $videojuego->getUsuario() !== $_usuario->getId()) : ?>
                    <a href="/mensaje/nuevo/<?= $videojuego->getUsuario() ?>" class="btn_1 ml-20"><?= _('Enviar mensaje') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!--================End Single Product Area =================-->