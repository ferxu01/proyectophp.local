<!-- Latest Products Start -->
<hr>
<section class="latest-product-area mt-40">
    <div class="container">
        <?php foreach ($videogames as $key => $videogame) : ?>
            <!-- Nav Card -->
            <br>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row">
                        <?php foreach ($videogame as $videojuego) : ?>
                            <?php include 'partials/videojuego.part.php'; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php include 'partials/paginacion.part.php'; ?>
            </div>
            <!-- End Nav Card -->
        <?php endforeach; ?>
    </div>
</section>
<!-- Latest Products End -->