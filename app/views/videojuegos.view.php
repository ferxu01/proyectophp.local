<div class="container">
    <div class="row justify-content-center align-content-center">
        <div class="col-md-12">
            <h2 class="text-center">
                <?= _('Nuevo producto') ?>
            </h2>
        </div>
        <div class="col-md-6">
            <form action="/videojuegos/nuevo" method="post" enctype="multipart/form-data">
                <?php include 'partials/error.part.php'; ?>
                <?php include 'partials/success.part.php'; ?>
                <div class="form-group">
                    <label for="nombre"><?= _('Nombre') ?></label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="<?php if(isset($_GET['nombre'])) echo $_GET['nombre'] ?>">
                </div>
                <div class="form-group">
                    <label for="descripcion"><?= _('Descripción') ?></label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="5" cols="5"><?php if(isset($_GET['descripcion'])) echo $_GET['descripcion'] ?></textarea>
                </div>
                <div class="form-group">
                    <label for="plataforma"><?= _('Plataforma') ?></label>
                    <select class="form-select" id="plataforma" name="plataforma">
                        <option value="0"><?= _('Seleccionar') ?></option>
                        <?php
                        foreach ($plataformas as $plataforma) : ?>
                            <option value="<?= $plataforma->getId() ?>"><?= $plataforma->getNombre() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group mt-70">
                    <label for="precio"><?= _('Precio') ?></label>
                    <input type="number" id="precio" name="precio" min="1" max="100" value="<?php if(isset($_GET['precio'])) echo $_GET['precio'] ?>">
                </div>
                <div class="form-group mt-30">
                    <label for="imagen"><?= _('Imagen') ?></label>
                    <input type="file" id="imagen" name="imagen">
                </div>
                <input type="submit" class="btn btn-primary" value="<?= _('Enviar') ?>">
            </form>
        </div>
    </div>
</div>