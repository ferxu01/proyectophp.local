<?php
namespace PROYECTOPHP\core;

use Exception;
use PROYECTOPHP\core\database\QueryBuilder;

class App
{
    private static $contenedor = [];

    public static function bind(string $key, $value)
    {
        static::$contenedor[$key] = $value;
    }

    public static function get(string $key)
    {
        if (!array_key_exists($key, static::$contenedor)) {
            throw new Exception("No se ha encontrado la clave $key en el contenedor");
        }
        return static::$contenedor[$key];
    }

    public static function getRepository(string $repositorio): QueryBuilder
    {
        if (!isset(static::$contenedor['repository']))
            static::$contenedor['repository'] = [];

        if (!isset(static::$contenedor['repository'][$repositorio]))
            static::$contenedor['repository'][$repositorio] = new $repositorio();

        return static::$contenedor['repository'][$repositorio];
    }
}