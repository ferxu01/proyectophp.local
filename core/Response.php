<?php
namespace PROYECTOPHP\core;

class Response
{
    public static function renderView(string $nombre, array $datos = [])
    {
        extract($datos);
        $_usuario = App::get('user');
        ob_start();
        require __DIR__ . "/../app/views/$nombre.view.php";
        $mainContent = ob_get_clean();
        require __DIR__ . '/../app/views/layout.view.php';
    }
}