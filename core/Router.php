<?php
namespace PROYECTOPHP\core;

use Exception;
use PROYECTOPHP\app\controllers\AuthController;
use PROYECTOPHP\app\controllers\PagesController;

class Router
{
    private $routes = [
        'GET' => [],
        'POST' => [],
        'DELETE' => []
    ];

    public function get(string $uri, string $controller, string $action, string $rol = 'ROL_ANONIMO')
    {
        $this->routes['GET'][$uri] = [
            'controller' => $controller . '@' . $action,
            'rol' => $rol
        ];
    }

    public function post(string $uri, string $controller, string $action, string $rol = 'ROL_ANONIMO')
    {
        $this->routes['POST'][$uri] = [
            'controller' => $controller . '@' . $action,
            'rol' => $rol
        ];
    }

    public function delete(string $uri, string $controller, string $action, string $rol = 'ROL_ANONIMO')
    {
        $this->routes['DELETE'][$uri] = [
            'controller' => $controller . '@' . $action,
            'rol' => $rol
        ];
    }

    public static function load(string $file)
    {
        $router = new static;
        App::bind('router', $router);
        require $file;
    }

    private function callAction(string $controller, string $action, array $parametros = [])
    {
        $objController = new $controller;
        if (!method_exists($objController, $action)) {
            throw new Exception(
                "El controlador $controller no responde al action $action"
            );
        }
        call_user_func_array([$objController, $action], $parametros);

        return true;
    }

    public function prepareRoute(string $route)
    {
        $urlRule = preg_replace(
            '/:([^\/]+)/',
            '(?<\1>[A-Za-z0-9\-\_]+)',
            $route
        );

        return str_replace('/', '\/', $urlRule);
    }

    private function getParametersRoute(string $route, array $matches)
    {
        preg_match_all('/:([^\/]+)/', $route, $parameterNames);

        return array_intersect_key($matches, array_flip($parameterNames[1]));
    }

    public function direct(string $uri, string $method)
    {
        foreach ($this->routes[$method] as $route => $data)
        {
            $urlRule = $this->prepareRoute($route);
            if (preg_match('/^' . $urlRule . '\/*$/s', $uri, $matches))
            {
                $controller = $data['controller'];
                $rol = $data['rol'];

                if (Security::isUserGranted($rol) === true) {
                    $parametros = $this->getParametersRoute($route, $matches);
                    list($controller, $action) = explode('@', $controller);
                    return $this->callAction($controller, $action, $parametros);
                } else {
                    if (!is_null(App::get('user')))
                        return $this->callAction(AuthController::class, 'unauthorized');
                    else
                        $this->redirect('login');
                }
            }
        }

        return $this->callAction(PagesController::class, 'notFound');
    }

    public function redirect(string $ruta)
    {
        header('location: /' . $ruta);

        exit();
    }
}