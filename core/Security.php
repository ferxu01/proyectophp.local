<?php
namespace PROYECTOPHP\core;


class Security
{
    public static function isUserGranted(string $rol): bool
    {
        if ($rol === 'ROL_ANONIMO')
            return true;

        $usuario = App::get('user');
        if (is_null($usuario))
            return false;

        $rolUsuario = $usuario->getRol();
        $roles = App::get('config')['security']['roles'];

        if ($roles[$rol] > $roles[$rolUsuario])
            return false;
        return true;
    }

    public static function encrypt(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function checkPassword(string $password, string $bdPassword): bool
    {
        return password_verify($password, $bdPassword);
    }
}