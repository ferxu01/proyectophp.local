<?php
use PROYECTOPHP\app\controllers\IdiomaController;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\database\Connection;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$idiomas = ['es_ES', 'en_GB'];

$lang = IdiomaController::getIdioma();

if (isset($_SESSION['idioma']))
    $language = $_SESSION['idioma'];
else if (array_search($lang, $idiomas))
    $language = $lang;
else
    $language = $idiomas[0];

putenv("LANGUAGE=$language");
putenv("LC_ALL=$language");
setlocale(LC_ALL, "$language.utf8");

bindtextdomain($language, __DIR__ . '/../locale');
bind_textdomain_codeset($language, 'UTF-8');
textdomain($language);

$config = require __DIR__ . '/../app/config.php';
App::bind('config', $config);

App::bind('connection', Connection::make($config['database']));