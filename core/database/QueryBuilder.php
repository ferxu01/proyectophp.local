<?php
namespace PROYECTOPHP\core\database;

use PROYECTOPHP\core\App;
use Exception;
use PDO;

abstract class QueryBuilder
{
    private PDO $connection;
    private string $tabla;
    private string $claseEntidad;

    /**
     * QueryBuilder constructor.
     * @param PDO $connection
     * @param string $tabla
     * @param string $claseEntidad
     */
    public function __construct(string $tabla, string $claseEntidad)
    {
        $this->connection = App::get('connection');
        $this->tabla = $tabla;
        $this->claseEntidad = $claseEntidad;
    }

    public function filterByDate(array $fechas = [], int $plataforma = null,
                                 string $ordenacion = 'ORDER BY fechaCreacion DESC'): array
    {

        //$sql = "SELECT * FROM $this->tabla WHERE fechaCreacion >= $fechas[fechaCreacionDesde] AND fechaCreacion <= $fechas[fechaCreacionHasta] $ordenacion;";
        $sql = "SELECT * FROM $this->tabla WHERE plataforma = $plataforma $ordenacion;";

        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function getNumRegistros()
    {
        $sql = "SELECT * FROM $this->tabla;";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        $numFilas = $pdoStatement->rowCount();
        return $numFilas;
    }

    public function findByLimit(int $desde, int $numRegistros, array $criterios): array
    {
        $strCriterios = implode(' AND ',
            array_map(
                function ($criterio) {
                    return $criterio . '=:' . $criterio;
                },
                array_keys($criterios)));

        $sql = sprintf("SELECT * FROM  %s WHERE %s LIMIT $desde, $numRegistros;",
            $this->tabla,
            $strCriterios
        );

        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute($criterios);
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function findAllLimit(int $desde, int $numRegistros, string $ordenacion = 'ORDER BY fechaCreacion DESC')
    {
        $sql = "SELECT * FROM $this->tabla $ordenacion LIMIT $desde, $numRegistros;";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function findOneBy(array $criterios): ?IEntity
    {
        $entities = $this->findBy($criterios);

        if (count($entities) > 1)
            throw new Exception('El método findOneBy está obteniendo más de un elemento como resultado');

        if (count($entities) === 1)
            return $entities[0];

        return null;
    }

    public function searchBy(array $criterios, string $ordenacion = 'ORDER BY fechaCreacion DESC'): array
    {
        $strCriterios = implode(' OR ',
            array_map(
                function ($criterio) {
                    return $criterio . ' LIKE :' . $criterio;
                },
                array_keys($criterios)));

        $sql = sprintf("SELECT * FROM  %s WHERE %s $ordenacion;",
            $this->tabla,
            $strCriterios
        );

        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute($criterios);
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function findBy(array $criterios): array
    {
        $strCriterios = implode(' AND ',
            array_map(
                function ($criterio) {
                    return $criterio . '=:' . $criterio;
                },
                array_keys($criterios)));

        $sql = sprintf("SELECT * FROM  %s WHERE %s;",
            $this->tabla,
            $strCriterios
        );

        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute($criterios);
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->tabla;";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->claseEntidad);
    }

    public function find(int $id): ?IEntity
    {
        $sql = "SELECT * FROM $this->tabla WHERE id=$id";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        $pdoStatement->setFetchMode(PDO::FETCH_CLASS, $this->claseEntidad);
        return $pdoStatement->fetch(PDO::FETCH_CLASS);
    }

    public function save(IEntity $entidad): bool
    {
        $parametros = $entidad->toArray();

        $campos = implode(', ', array_keys($parametros));
        $valores = ':' . implode(', :', array_keys($parametros));
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s);",
            $this->tabla,
            $campos,
            $valores
        );
        $pdoStatement = $this->connection->prepare($sql);
        return $pdoStatement->execute($parametros);
    }

    public function update(IEntity $entidad): bool
    {
        $parametros = $entidad->toArray();

        $campos = '';
        foreach ($parametros as $nombre => $valor)
            $campos .= "$nombre=:$nombre, ";
        $campos = rtrim($campos, ', ');

        $sql = sprintf(
            "UPDATE %s SET %s WHERE id = %s;",
            $this->tabla,
            $campos,
            $entidad->getId()
        );
        $pdoStatement = $this->connection->prepare($sql);
        return $pdoStatement->execute($parametros);
    }

    public function delete(IEntity $entidad): bool
    {
        $sql = sprintf(
            "DELETE FROM %s WHERE id = :id;",
            $this->tabla
        );
        $pdoStatement = $this->connection->prepare($sql);

        $id = $entidad->getId();
        $pdoStatement->bindParam('id', $id);
        return $pdoStatement->execute();
    }

    public function getUltimaCompra(): int
    {
        $sql = "SELECT idCompra FROM $this->tabla";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        $numRows = $pdoStatement->rowCount();
        return $numRows;
    }

    public function getUltimoMensaje(): int
    {
        $sql = "SELECT id FROM $this->tabla";
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
        $numRows = $pdoStatement->rowCount();
        return $numRows;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }
}