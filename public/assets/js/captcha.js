'use strict';

document.addEventListener('DOMContentLoaded', e => {
    let botonRecargarCaptcha = document.getElementById('recargarCaptcha');

    botonRecargarCaptcha.addEventListener('click', e => {
        document.querySelector('.imagen-captcha').src = '/generarCaptcha';
    });
});