'use strict';

document.addEventListener('DOMContentLoaded', e => {
    let botonesDeleteJson = document.querySelectorAll('#btnBorrarJson');

    botonesDeleteJson.forEach(boton => {
        boton.addEventListener('click', function (e) {
            e.preventDefault();

            fetch(this.href, {method: 'DELETE'})
                .then(respuesta => respuesta.json())
                .then(resp => {
                    this.parentElement.parentElement.remove();
                })
                .catch(error => {
                    let rutaSeparada = this.href.split('/');
                    if (rutaSeparada[3] === 'usuario') {
                        alert('El usuario no se pudo borrar');
                    } else if (rutaSeparada[3] === 'videojuego') {
                        alert('El videojuego no se pudo borrar');
                    }
                });
        });
    });

    //Buscar videojuegos
    let btnSearch = document.getElementById('btnSearch');

    btnSearch.addEventListener('click', function(e) {
        e.preventDefault();

        let searchValue = document.getElementById('searchInput').value;
        fetch('/videojuegos/buscar', {
            method: 'POST',
            body: JSON.stringify({ 'search': searchValue })
        }).then(resp => resp.json())
            .then(juegos => {
                let contenedorMostrarBusqueda = document.getElementById('datosJsonBuscar');
                contenedorMostrarBusqueda.innerHTML = '';
                let template = '';

                for (let i=0; i<juegos.length; i++) {
                    template += `
                        <div class="card">
                            <img class="card-img-top" src="${juegos[i].imagen}" alt="${juegos[i].nombre}">
                            <div class="card-body">        
                                <a href="${juegos[i].linkDetallesVideojuego}"><h5>${juegos[i].nombre}</h5></a>
                                <p class="card-text">${juegos[i].fechaCreacion}</p>
                            </div>
                            <div class="row justify-content-around">
                                <p class="card-text">${juegos[i].plataforma}</p>
                                <p class="card-text">${juegos[i].usuario}</p>
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-around">
                                    <p class="card-text text-left">${juegos[i].precio} €</p>
                                    <a href="${juegos[i].linkDetallesVideojuego}"><button class="btn-sm btn-primary">Más info</button></a>
                                </div>
                            </div>
                        </div>
                    `;

                    contenedorMostrarBusqueda.innerHTML = template;
                }
            })
    });
});