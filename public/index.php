<?php
use PROYECTOPHP\app\repository\UsuarioRepository;
use PROYECTOPHP\core\App;
use PROYECTOPHP\core\Request;
use PROYECTOPHP\core\Router;

require __DIR__ . '/../core/bootstrap.php';

if (isset($_SESSION['usuario'])) {
    $usuario = App::getRepository(UsuarioRepository::class)->find($_SESSION['usuario']);
    if (is_null($usuario)) {
        App::get('router')->redirect('login');
    }
    $_SESSION['idioma'] = $usuario->getIdioma();
} else {
    $usuario = null;
}
App::bind('user', $usuario);

Router::load(__DIR__ . '/../app/routes.php');

App::get('router')->direct(Request::uri(), Request::method());